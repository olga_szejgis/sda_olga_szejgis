//14. Wczytaj dane z pliku z list� kurs�w i ich dni/godzin
//    Format: [nazwa] [dzien] [poczatek] [koniec]
//    a)Wypisz podany plan uszeregowany wed�ug dnia tygodnia a nast�pnie godziny rozpocz�cia
//    b)Wypisz wszystkie kursy, kt�re maj� ze sob� konflikt godzin.

#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <cstdlib>

using namespace std;

struct Time
{
	int mHour;
	int mMinute;

	Time(int hour, int minute)
	:mHour(hour)
	,mMinute(minute)
	{
	}

	bool operator <( const Time second) const
	{
		if(this->mHour==second.mHour)
		{
			return this->mMinute<second.mMinute;
		}
		return this->mHour<second.mHour;
	}

	bool operator >( const Time second) const
	{
		if (this->mHour == second.mHour)
		{
			return this->mMinute > second.mMinute;
		}
		return this->mHour > second.mHour;
	}
	bool operator >=( const Time second) const
	{
		if (this->mHour == second.mHour)
		{
			return this->mMinute >= second.mMinute;
		}
		return this->mHour >= second.mHour;
	}
	bool operator <=( const Time second) const
	{
		if (this->mHour == second.mHour)
		{
			return this->mMinute <= second.mMinute;
		}
		return this->mHour <= second.mHour;
	}
	bool operator ==( const Time second) const
	{
		if (this->mHour == second.mHour)
		{
			return this->mMinute == second.mMinute;
		}
		return this->mHour == second.mHour;
	}
};

struct Course
{
	string mName;
	int mDay;
	Time mStart;
	Time mEnd;

	Course(string name, int day, int startHour, int startMinute, int endHour, int endMinute)
	:mName(name)
	,mDay(day)
	,mStart(startHour, startMinute)
	,mEnd(endHour, endMinute)
	{
	}
};

bool ifCollision(Course first, Course second)
{
	if (((first.mStart> second.mStart)&& (first.mStart< second.mEnd))
				|| ((first.mEnd > second.mStart)&& (first.mEnd < second.mEnd))
				|| ((first.mStart <= second.mStart)&& (first.mEnd>= second.mEnd))
				|| ((first.mStart == second.mStart)&& (first.mEnd == second.mEnd)))
	{
		return true;
	}
	return false;
}

void checkCollision(vector<Course> toCheck)
{
	for(vector<Course>::iterator it=toCheck.begin(); it!=toCheck.end(); advance(it,1))
	{
		for(vector<Course>::iterator it2=it+1; it2!=toCheck.end(); advance(it2,1))
		{
			if(it->mDay==it2->mDay)
			{

				if(ifCollision(*it, *it2))
				{
					cout<<"Collision!"<<endl;
					cout<<it->mName<<" "<<it->mStart.mHour<<" "<<it->mStart.mMinute<<" "<<it->mEnd.mHour<<" "<<it->mEnd.mMinute<<endl;
					cout<<it2->mName<<" "<<it2->mStart.mHour<<" "<<it2->mStart.mMinute<<" "<<it2->mEnd.mHour<<" "<<it2->mEnd.mMinute<<endl;
				}
			}
		}
	}
}

bool sortCourse(Course first, Course second)
{
	if(first.mDay==second.mDay)
	{
		return first.mStart<second.mStart; // trzeba nadpisac operator porownania dla Time
	}
	else
	{
		return first.mDay<second.mDay;
	}
}

void setStart(int start, int& startHour, int& startMinute)
{
	string tmp;
	string str;

	sprintf((char*) tmp.c_str(), "%d", start);
	str = tmp.c_str();

	startHour= (str[1]-'0')+((str[0]-'0')*10);
	startMinute= (str[3]-'0')+((str[2]-'0')*10);
}

void setEnd(int end, int& endHour, int& endMinute)
{
	string tmp;
	string str;

	sprintf((char*) tmp.c_str(), "%d", end);
	str = tmp.c_str();

	endHour= (str[1]-'0')+((str[0]-'0')*10);
	endMinute= (str[3]-'0')+((str[2]-'0')*10);
}

void courseCollision(vector<Course>toCheck)
{

}

int main()
{
	vector<Course> courses;

	ifstream file;
	file.open("data.txt");

    if (!file.good())
    {
        cerr<< "Error reding file"<< endl;
        return 1;
    }

    string name;
    int day;
    int start;
    int end;
    int startHour;
    int startMinute;
    int endHour;
    int endMinute;

    while (!file.eof())
	{
		file >> name >> day >> start >> end;
		setStart(start, startHour, startMinute);
		setEnd(end, endHour, endMinute);
		courses.push_back(Course(name, day, startHour, startMinute, endHour, endMinute));
	}

    file.close();

    sort(courses.begin(),courses.end(), sortCourse);

    for(vector<Course>::iterator it=courses.begin(); it!=courses.end(); advance(it, 1))
  	{
  		cout << it->mName<< " " << it->mDay << " "<< it->mStart.mHour << " "<< it->mStart.mMinute<<" " << it->mEnd.mHour << " "<< it->mEnd.mMinute<<endl;
  	}

    checkCollision(courses);


	return 0;
}
