/*
 * factoryamd.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef FACTORYAMD_HPP_
#define FACTORYAMD_HPP_
#include "abstractfactory.hpp"

class FactoryAMD:public Factory
{
public:
	Processor* createProcessor()
	{
		return new ProcessorAMD;
	}
	Cooler* createCooler()
	{
		return new CoolerAMD;
	}
};




#endif /* FACTORYAMD_HPP_ */
