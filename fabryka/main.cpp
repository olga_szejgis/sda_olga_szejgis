#include <iostream>
#include "factoryintel.hpp"
#include "factoryamd.hpp"
#include "cooleramd.hpp"
#include "coolerintel.hpp"
#include "processorintel.hpp"
#include "processoramd.hpp"
#include "computer.hpp"

int main()
{
	FactoryIntel factoryIntel;
	FactoryAMD factoryAMD;
	Computer intelPC("PC1 ", factoryIntel);
	Computer amdPC("PC2 ", factoryAMD);
	intelPC.run();
	amdPC.run();

	return 0;

}



