/*
 * processoramd.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef PROCESSORAMD_HPP_
#define PROCESSORAMD_HPP_
#include "processor.hpp"

class ProcessorAMD: public Processor
{
public:
	void process()
	{
		std::cout<< "Pracuje" << std::endl;
	}
	~ProcessorAMD()
	{
		std::cout << "~Processor AMD"<< std::endl;
	}
};


#endif /* PROCESSORAMD_HPP_ */
