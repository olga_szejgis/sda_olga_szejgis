/*
 * factoryintel.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef FACTORYINTEL_HPP_
#define FACTORYINTEL_HPP_
#include "abstractfactory.hpp"

class FactoryIntel:public Factory
{
public:
	Processor* createProcessor()
	{
		return new ProcessorIntel;
	}
	Cooler* createCooler()
	{
		return new CoolerIntel;
	}
};



#endif /* FACTORYINTEL_HPP_ */
