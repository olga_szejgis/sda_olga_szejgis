/*
 * cooler.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef COOLER_HPP_
#define COOLER_HPP_
#include <iostream>

class Cooler
{
public:
	Cooler()
{}

	virtual void cool()=0;
	virtual~Cooler()
	{
		std::cout << "~Cooler"<< std::endl;
	}
};



#endif /* COOLER_HPP_ */
