/*
 * computer.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef COMPUTER_HPP_
#define COMPUTER_HPP_
#include <iostream>

class Computer
{
protected:
	std::string mName;
	Processor* mProcessor;
	Cooler* mCooler;
public:
	Computer(std::string name, Factory& factory)
:mName(name)
{
mProcessor=factory.createProcessor();
mCooler=factory.createCooler();
}
	~Computer()
	{
		delete mProcessor;
		delete mCooler;
	}
	void run()
	{
		std::cout << "Nazywam sie: " << mName<< std::endl;
		mProcessor->process();
		mCooler->cool();
	}
};



#endif /* COMPUTER_HPP_ */
