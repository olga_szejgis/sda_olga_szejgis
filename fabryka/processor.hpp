/*
 * processor.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef PROCESSOR_HPP_
#define PROCESSOR_HPP_
#include <iostream>

class Processor
{
public:
	Processor()
{
}
	virtual void process()=0;
	virtual~Processor()
	{
		std::cout << "~Processor"<< std::endl;
	}
};



#endif /* PROCESSOR_HPP_ */
