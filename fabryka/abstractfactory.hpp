/*
 * abstractfactory.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef ABSTRACTFACTORY_HPP_
#define ABSTRACTFACTORY_HPP_
#include <iostream>
#include "cooleramd.hpp"
#include "coolerintel.hpp"
#include "processorintel.hpp"
#include "processoramd.hpp"

class Factory
{
public:
	Factory()
{

}
	virtual Processor* createProcessor()=0;
	virtual Cooler* createCooler()=0;
	virtual~Factory()
	{

	}
};



#endif /* ABSTRACTFACTORY_HPP_ */
