/*
 * unit.hpp
 *
 *  Created on: 27.04.2017
 *      Author: RENT
 */

#ifndef UNIT_HPP_
#define UNIT_HPP_
#include <iostream>
#include <ctime>
#include <cstdlib>

class Group;

class Unit
{
std::string mID;
Group* mGroupPtr;

public:
Unit(std::string ID);
void printID();
void replicate();
void addToGroup(Group* groupPtr);
};



#endif /* UNIT_HPP_ */
