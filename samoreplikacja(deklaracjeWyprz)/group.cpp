/*
 * group.cpp
 *
 *  Created on: 27.04.2017
 *      Author: RENT
 */
#include "group.hpp"
#include "unit.hpp"


Group::Group()
:mUnits(0)
,mSize(0)
{
}
void Group::resize ()
{
if (mSize==0)
{
mSize++;
mUnits= new Unit*[mSize];
}
else
{
	mSize++;
	Unit** newUnitsArray= new Unit*[mSize];
	for (unsigned int i=0; i<mSize-1;i++)
	{
		newUnitsArray[i]=mUnits[i];
	}
	delete [] mUnits;
	mUnits=newUnitsArray; //zmienna newUnits usuwa sie sama ze stosu
}
}
void Group:: add (Unit* unit)
{
resize();
mUnits[mSize-1]= unit;
}
void Group::clear()
{
	for (unsigned int i=0; i<mSize; i++)
	{
		delete mUnits[i]; // kasowanie wskaznikow
	}
	delete[] mUnits; //delete tablicowy
	mSize=0;
	mUnits=0;
}
void Group::replicateGroup()
{
	unsigned int currentSize= mSize;
	for (unsigned int i=0; i<currentSize; i++)
{
	mUnits[i]->replicate();
}
}
void Group::printUnits()
{
	for (unsigned int i=0; i<mSize; i++)
	{
	mUnits[i]->printID();
	}
}
Group::~Group()
{
clear();
}
