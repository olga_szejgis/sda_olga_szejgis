/*
 * group.hpp
 *
 *  Created on: 27.04.2017
 *      Author: RENT
 */

#ifndef GROUP_HPP_
#define GROUP_HPP_

class Unit;

class Group
{
	Unit** mUnits;
	unsigned int mSize;
	void resize ();

	public:
	Group();
	void add (Unit* unit);
	void clear();
	void replicateGroup();
	void printUnits();
	~Group();
};



#endif /* GROUP_HPP_ */
