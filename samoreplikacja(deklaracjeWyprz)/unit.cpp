/*
 * unit.cpp
 *
 *  Created on: 27.04.2017
 *      Author: RENT
 */
#include "unit.hpp"
#include "group.hpp"

Unit::Unit(std::string ID)
:mID(ID)
,mGroupPtr(0)
{
}
void Unit:: printID()
{
	std::cout << " Units ID: " << mID << std::endl;
}
void Unit::replicate()
{
srand(time(NULL));
int idVariation= rand() %10;
char newID='0'+ idVariation;

Unit* newborn= new Unit (mID + newID);
newborn->addToGroup(mGroupPtr);
//mGroupPtr->add(newborn);
}

void Unit::addToGroup(Group* group)
{
mGroupPtr=group;
mGroupPtr->add(this);
}
