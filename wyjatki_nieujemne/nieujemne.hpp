/*
 * nieujemne.hpp
 *
 *  Created on: 15.05.2017
 *      Author: RENT
 */

#ifndef NIEUJEMNE_HPP_
#define NIEUJEMNE_HPP_
#include <iostream>

class Nieujemne
{
	int value;

public:
	int getValue() const
	{
		return value;
	}

	void setValue(int value)
	{
		this->value = value;
	}
	Nieujemne(int v)
	{
		if (value<0)
		{
			std::string blad("Podano liczbe ujemna ");
			throw blad;
		}
		value=v;
	}

	Nieujemne operator- (Nieujemne druga)
	{
		if (this->getValue()-druga.getValue()<0)
		{
			std::string blad ("Liczba ujemna!");
			throw blad;
		}
			return this->getValue()-druga.getValue();
	}
	Nieujemne operator/ (Nieujemne druga)
		{
		if (druga.getValue()==0)
		{
			std::string blad("Nie dziel przez 0!");
			throw blad;
		}
			return this->getValue()/druga.getValue();
		}
	Nieujemne operator* (Nieujemne druga)
		{
		return this->getValue()* druga.getValue();
		}
};



#endif /* NIEUJEMNE_HPP_ */
