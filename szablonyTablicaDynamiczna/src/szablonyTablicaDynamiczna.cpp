//============================================================================
// Name        : szablonyTablicaDynamiczna.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <typeinfo>
using namespace std;


template<typename type>

class DynamicArray
{
	type* mValue;
	int mSize;
	void resize()
	{
		if (mSize==0)
		{
			mSize++;
			mValue= new type[mSize];
		}
		else
		{
			mSize++;
			type* tmpArray= new type[mSize];
			for (int i=0; i<mSize-1; i++)
			{
				tmpArray[i]= mValue[i];
			}
			delete[]mValue;
			mValue=tmpArray;
		}
	}

public:
	DynamicArray()
:mValue(0)
,mSize(0)
{
}
DynamicArray(int size)
:mSize(size)
{
	mValue= new type[mSize];
}

void add(const type& value)
{
	resize();
	mValue[mSize-1]=type(value);

}
	int find (const type& value)
	{
		for (int i=0; i<mSize; i++)
		{
			if(get(i)==value)
			{
				return i;
			}

		}
		return -1;
	}
	int size()
	{
		return mSize;
	}
	bool empty()
	{
		return(mSize==0)?true:false;
	}
	void remove(const type& value)
	{
		int pos=find(value);
		if (pos>-1)
		{
			remove(pos);
		}
		else
		{
//nic
		}
	}
	void remove (int pos)
	{
		if (pos<mSize&& pos>=0)
		{
			type* tmpArray= new type[mSize-1];

			for (int i=0; i<mSize-1;i++)
			{
				if(i<pos)
				{
					tmpArray[i]=mValue[i];
				}
				else if (i>=pos)
				{
					tmpArray[i]= mValue[i+1];
				}
			}
			delete[] mValue;
			mValue=tmpArray;
			mSize--;
		}
		else
		{
			throw out_of_range("Indeks poza zakresem");
		}
	}
	void clear()
	{
		delete[]mValue;
		mSize=0;
		mValue=0;
	}
	type get(int pos)
	{
		if(pos<mSize && pos>0)
		{
		return mValue[pos];
		}
		else
		{
			throw out_of_range("Indeks poza zakresem");
		}

	}
	type operator[] (int pos)
	{
		return get(pos);
	}
	void showAll()
	{
		cout<<"[";
		for (int i=0; i<mSize;i++)
		{
			cout << mValue[i];
		}
		cout<<"]"<<endl;
	}
	~DynamicArray()
	{
		clear();
	}

};

int main()
{
	DynamicArray<int>tabl(3);
	cout << "Rozmiar tablicy: "<< tabl.size()<< endl;
	tabl.add(11);
	tabl.add(5);
	tabl.add(7);
	tabl.showAll();
	cout << "Rozmiar tablicy: "<< tabl.size()<< endl;


	return 0;
}
