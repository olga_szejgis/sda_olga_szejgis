#include "dzikieZwierze.hpp"
#include "kot.hpp"
#include "pies.hpp"
#include "kotopies.hpp"


int main()
{
	Kot k1 ("Fruzia");
	Kot k2 ("Mruczek");
	Kot k3 ("Bezczel");
	Pies p1 ("Burek");
	Pies p2 ("Maniek");
	Pies p3 ("Puszek");
	KotoPies kp1 ("Janusz");

	k1.dajGlos();
	p1.dajGlos();
	kp1.dajGlos();

	DzikieZwierze* Zwierzaki[6];
	Zwierzaki[0]= &k1;
	Zwierzaki[1]= &p1;
	Zwierzaki[2]= &k2;
	Zwierzaki[3]= &p2;
	Zwierzaki[4]= &kp1;
	Zwierzaki[5]= &p3;

	for(int i=0; i<6; ++i)
	{
		Zwierzaki[i]->dajGlos();
	}
	return 0;
}


