/*
 * dzikieZwierze.hpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef DZIKIEZWIERZE_HPP_
#define DZIKIEZWIERZE_HPP_
#include <iostream>

class DzikieZwierze
{
protected:
	std::string mImie;
public:
	virtual void dajGlos()=0;
	virtual ~DzikieZwierze();
	DzikieZwierze(std::string imie);
	DzikieZwierze();
};




#endif /* DZIKIEZWIERZE_HPP_ */
