/*
 * kotopies.hpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef KOTOPIES_HPP_
#define KOTOPIES_HPP_
#include "kot.hpp"
#include "pies.hpp"

class KotoPies: public Kot, Pies
{
public:
	KotoPies(std::string imie);
	void virtual dajGlos();


};




#endif /* KOTOPIES_HPP_ */
