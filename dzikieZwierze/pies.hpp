/*
 * pies.hpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef PIES_HPP_
#define PIES_HPP_
#include "dzikieZwierze.hpp"

class Pies: public virtual DzikieZwierze
{
public:
	void virtual dajGlos();
	Pies(std::string imie);
	Pies();
};


#endif /* PIES_HPP_ */
