/*
 * kot.hpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef KOT_HPP_
#define KOT_HPP_
#include "dzikieZwierze.hpp"

class Kot: public virtual DzikieZwierze
{
public:
	void virtual dajGlos();
	Kot(std::string imie);
	Kot();
};




#endif /* KOT_HPP_ */
