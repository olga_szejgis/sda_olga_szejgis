/*
 * szyfrowanyTekst.hpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#ifndef SZYFROWANYTEKST_HPP_
#define SZYFROWANYTEKST_HPP_
#include "wypisywalny.hpp"

class SzyfrowanyTekst: public Wypisywalny
{
protected:
	bool mCzySzyfrowany;

public:
	virtual void szyfruj()=0;
	virtual void wypisz()=0;
	SzyfrowanyTekst()
	:mCzySzyfrowany(false)
	{
		std:: cout << "Szyfrowany tekst - konstruktor " << std::endl;
	}
	virtual ~SzyfrowanyTekst()
	{
		std:: cout << "Szyfrowany tekst - destruktor " << std::endl;
	}
};


#endif /* SZYFROWANYTEKST_HPP_ */
