/*
 * wypisywalny.cpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#include "wypisywalny.hpp"

Wypisywalny::Wypisywalny()
:mLancuch("kaczuszka")
{
	std::cout << "Tworzem " << mLancuch << std:: endl;
}
Wypisywalny::Wypisywalny (std::string lancuch)
:mLancuch(lancuch)
{
	std::cout << "Tworzem " << mLancuch << std:: endl;
}

const std::string& Wypisywalny::getLancuch() const
{
	return mLancuch;
}

void Wypisywalny::setLancuch(const std::string& lancuch)
{
	mLancuch = lancuch;
}

Wypisywalny::~Wypisywalny()
{
	std::cout << "Niszczem " << mLancuch << std:: endl;
}

void Wypisywalny::wypisz()
{
	std::cout << mLancuch << std:: endl;
}
