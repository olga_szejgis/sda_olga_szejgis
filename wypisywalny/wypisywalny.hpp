/*
 * wypisywalny.hpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#ifndef WYPISYWALNY_HPP_
#define WYPISYWALNY_HPP_
#include <iostream>

class Wypisywalny
{
protected:
	std::string mLancuch;

public:
	Wypisywalny();
	Wypisywalny (std::string lancuch);
	~Wypisywalny();
	const std::string& getLancuch() const;
	void setLancuch(const std::string& lancuch);
	void wypisz();

};



#endif /* WYPISYWALNY_HPP_ */
