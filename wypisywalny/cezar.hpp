/*
 * cezar.hpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#ifndef CEZAR_HPP_
#define CEZAR_HPP_
#include "szyfrowanyTekst.hpp"

class Cezar: public SzyfrowanyTekst
{
public:
	Cezar(std::string lancuch);
	~Cezar();
	void szyfruj();
	void wypisz();
};


#endif /* CEZAR_HPP_ */
