//============================================================================
// Name        : nieparzyste.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>

void wyswietl (int rozmiar, int* tablica, int& iloscWynikow)
{
	iloscWynikow=0;

	for (int i = 1, iTablicy = 0; i < rozmiar; i += 2, iTablicy += 1)

		{
				tablica[iTablicy] = i;
				iloscWynikow += 1;
		}
}

int main()
{
	int rozmiar;
	int ileWyszlo;

	std::cout << "Podaj gorna granice wyswietlania: " << std::endl;
	std::cin >> rozmiar;

	int* tablica= new int [rozmiar/2+1];

	wyswietl (rozmiar, tablica, ileWyszlo);

	std::cout << "Liczby nieparzyste (" << ileWyszlo << "):" << std::endl;

		for (int i = 0; i < ileWyszlo; ++i)

		{
			std::cout << tablica[i] << ' ';
		}

	delete [] tablica;

	return 0;
}
