

#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

class Kostka
{
	int mScianki;
	int mWartosc;

public:

	Kostka (int scianki)
		:mScianki(scianki)
		,mWartosc()
		{
		srand(time(NULL));
		this-> wylosuj();
		}

	int wylosuj ()
	{
		mWartosc = rand() % mScianki+1;

		return mWartosc;
	}

	int getWartosc ()
	{
		return mWartosc;
	}
};

int main()
{
	Kostka k1(7);
	cout << k1.wylosuj() << endl;
	Kostka k2(8);
	cout << k2.wylosuj() << endl;
	cout << k2.getWartosc()<< endl;

	return 0;
}
