//============================================================================
// Name        : kontenery_zadanie13_palindrom.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
using namespace std;

int changeToNumber(int toConvert)
{
	while(toConvert>9)
	{
		toConvert=toConvert/10;
	}
	return toConvert;
}
vector<int> intToVector (int toConvert)
{
	vector<int> converted;
	int rest;
	int toDivideBy=10;

	while(toConvert!=0)
	{
		rest=toConvert%toDivideBy;
		toConvert-=rest;
		toDivideBy*=10;
		converted.push_back(changeToNumber(rest));
	}
	reverse(converted.begin(), converted.end());
	return converted;
}

bool ifPalindrome(vector<int> toCheck)
{
	return equal(toCheck.begin(), toCheck.begin() + toCheck.size()/2, toCheck.rbegin());
}

vector<int> findPalindrome(int toCheck)
{
	vector<int> tmp= intToVector(++toCheck);

	while(!ifPalindrome(tmp))
	{
		tmp= intToVector(++toCheck);
	}
	return tmp;
}

int main()
{
	vector<int>myVector=findPalindrome(12);
	for (vector<int>::iterator it=myVector.begin(); it!=myVector.end(); ++it)
						{
							cout << *it<< " ";
						}

	return 0;
}
