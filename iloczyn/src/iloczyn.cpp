//============================================================================
// Name        : iloczyn.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>

void wypelnij (int *tab, int rozmiar)
{
	int liczba;

	for (int i=0; i<rozmiar; ++i)
	{
		std::cout << "Podaj " << i+1 << " liczbe " << std::endl;
		std::cin >> liczba;

		if (liczba==0)
		{
			std::cout << "Liczba nie moze byc zerem! " << std:: endl;
			i--;
		}
		else
		{
			tab[i]= liczba;
		}
	}
}

int iloczyn (int* tab, int rozmiar)
{
	int iloczyn=1;

	for (int i=0; i<rozmiar; ++i)
	{
		iloczyn*=tab[i];
	}
	return iloczyn;
}

int main()
{
	int ilosc;
	std::cout << "Podaj ile liczb chcesz pomonozyc: " << std::endl;
	std::cin >> ilosc;

	int* tab= new int [ilosc];

	wypelnij (tab, ilosc);
	std:: cout << "Iloczyn to: " << iloczyn(tab, ilosc) << std::endl;


	return 0;
}
