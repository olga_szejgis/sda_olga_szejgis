//============================================================================
// Name        : Kostka.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

class Kostka
{
	int mScianki;
	int mWartosc;

public:

	Kostka (int scianki)

		{
		mScianki=scianki;
		mWartosc=0;
		}

	int wylosuj ()
	{
		mWartosc = rand() % mScianki+1;

		return mWartosc;
	}
};

int main()
{
	srand(time(NULL));
	Kostka k1(7);
	cout << k1.wylosuj() << endl;
	Kostka k2(8);
	cout << k2.wylosuj() << endl;


	return 0;
}
