//12. Napisz funkcj�, kt�ra zamieni podanego int na vector jej cyfr.

#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
using namespace std;

int changeToNumber(int toConvert)
{
	while(toConvert>9)
	{
		toConvert=toConvert/10;
	}
	return toConvert;
}
vector<int> intToVector (int toConvert)
{
	vector<int> converted;
	int rest;
	int toDivideBy=10;

	while(toConvert!=0)
	{
		rest=toConvert%toDivideBy;
		toConvert-=rest;
		toDivideBy*=10;
		converted.push_back(changeToNumber(rest));
	}
	reverse(converted.begin(), converted.end());
	return converted;
}

int main()
{

	vector <int> myVector=intToVector(8623);

	for (vector<int>::iterator it=myVector.begin(); it!=myVector.end(); ++it)
					{
						cout << *it<< endl;
					}

	return 0;
}
