/*
 * cylinder.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef CYLINDER_HPP_
#define CYLINDER_HPP_
#include "circle.hpp"

class Cylinder: public Circle
{
public:
	float pole();
	Cylinder(float wysokosc, float promien, float pi);
	std::string getNazwa();
protected:
	float mWysokosc;
};


#endif /* CYLINDER_HPP_ */
