/*
 * shape.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef SHAPE_HPP_
#define SHAPE_HPP_
#include <iostream>

class Shape
{
protected:
	std::string mNazwa;
public:
	virtual float pole()=0;
	virtual std::string getNazwa()=0;
	virtual~Shape()
	{
	}
};



#endif /* SHAPE_HPP_ */
