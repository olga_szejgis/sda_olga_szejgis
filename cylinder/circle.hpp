/*
 * circle.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef CIRCLE_HPP_
#define CIRCLE_HPP_
#include "shape.hpp"

class Circle: public Shape
{
protected:
	float mPromien;
	const float mPi;

public:
	float pole();
	std::string getNazwa();
	Circle(float promien, float pi=3.14);
	Circle();
};


#endif /* CIRCLE_HPP_ */
