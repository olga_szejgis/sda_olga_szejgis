/*
 * Tile.hpp
 *
 *  Created on: 03.06.2017
 *      Author: RENT
 */

#ifndef TILE_HPP_
#define TILE_HPP_

namespace Game
{
struct Tile
{
	int mX;
	int mY;
	bool isWall;

	Tile(int x, int y, bool wall)
	:mX(x)
	,mY(y)
	,isWall(wall)
	{
	}
};
}

#endif /* TILE_HPP_ */
