/*
 * Player.cpp
 *
 *  Created on: 05.06.2017
 *      Author: RENT
 */
#include "Player.hpp"

namespace Game
{
Player::Player()
:Actor()
,mHP(5)
{
}
Player::Player(int x, int y, char symbol, Game::FgColour::Colour colourF, Game::BgColour::Colour colourB, RogueGame* game, int hp)
:Actor(x, y, symbol, colourF, colourB, game)
,mHP(hp)
{
}
void Player::changeHP(int HPchange)
{
	mHP+=HPchange;
}

}
