/*
 * Map.hpp
 *
 *  Created on: 03.06.2017
 *      Author: RENT
 */

#ifndef MAP_HPP_
#define MAP_HPP_
#include "Tile.hpp"
#include"Console.hpp"
#include <vector>

namespace Game
{
class RogueGame;
class Map
{
	std::vector<Tile> mTiles;
	void drawWall(const Game::Console& console, int x, int y);
	void drawFloor(const Game::Console& console, int x, int y);
	RogueGame* mGame;
public:
	Map();
	Map(int width, int height);
	bool isWall(int x, int y);
	void setTile(int x, int y, bool wall);
	void render(Game::Console& console);
};
}


#endif /* MAP_HPP_ */
