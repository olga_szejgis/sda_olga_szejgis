/*
 * Game.cpp
 *
 *  Created on: 05.06.2017
 *      Author: RENT
 */
#include "Game.hpp"

namespace Game
{
RogueGame::RogueGame()
{
	mConsole=Console();
	short int w = 100;
	short int h = 40;
	mPlayer= Player(10, 5, '$',FgColour::YELLOW, BgColour::BLACK, this, 10);
	mMap=Map(w/2, h/2);
	mEnemies.push_back(Enemy(15, 7, '#', FgColour::RED, BgColour::BLACK, this));
	mEnemies.push_back(Enemy(10, 7, '#', FgColour::RED, BgColour::BLACK, this));
	mEnemies.push_back(Enemy(12, 5, '#', FgColour::RED, BgColour::BLACK, this));
}

void RogueGame::init(const Game::Console& console)
{
	short int w = 100;
	short int h = 40;

	mConsole.setConsoleSize(w, h);

		for(int x=0; x<w/2; ++x)
		{
			for(int y=0; y<h/2; ++y)
			{
				if(x==0||y==0||x==(w/2-1)||y==(h/2-1))
				{
					mMap.setTile(x, y, true);
				}
			}
		}
}

void RogueGame::draw()
{
	mConsole.clear();
	mMap.render(mConsole);
	mPlayer.render(mConsole);
	for(auto it=mEnemies.begin(); it!=mEnemies.end(); it++)
	{
		it->render(mConsole);
	}
}

void RogueGame::update(Key key)
{
	int dX=0;
	int dY=0;

	switch (key)
	{
	case Game::Key::MOVE_UP:
		dY=-1;
		break;
	case Game::Key::MOVE_DOWN:
		dY=1;
		break;
	case Game::Key::MOVE_LEFT:
		dX=-1;
		break;
	case Game::Key::MOVE_RIGHT:
		dX=1;
		break;
	case Game::Key::QUIT:
	case Game::Key::INVALID:
	default:
		//Nothing
		break;
	}

	for(auto it=mEnemies.begin(); it!=mEnemies.end(); it++)
	{
		it->updateEnemy();
	}

	mPlayer.move(dX, dY);

	for(auto it=mEnemies.begin(); it!=mEnemies.end(); it++)
	{
		if (mPlayer.checkCollision(*it))
		{
			mPlayer.changeHP(-1);
			mEnemies.erase(it);
			it--;
		}
	}
}
void RogueGame::gameLoop(Key key)
{
	while (key != Key::QUIT)
		{
			draw();
			key = mConsole.getKey();
			update(key);

		}
}
bool RogueGame::canMove(int x, int y)
{
	return !mMap.isWall(x, y);
}
}


