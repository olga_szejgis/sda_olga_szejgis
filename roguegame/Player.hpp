/*
 * Player.hpp
 *
 *  Created on: 05.06.2017
 *      Author: RENT
 */

#ifndef PLAYER_HPP_
#define PLAYER_HPP_
#include "Actor.hpp"

namespace Game
{
class Player: public Actor
{
int mHP;
public:
Player();
Player(int x, int y, char symbol, Game::FgColour::Colour colourF, Game::BgColour::Colour colourB, RogueGame* game, int hp);
void changeHP(int HPchange);
};
}



#endif /* PLAYER_HPP_ */
