/*
 * Map.cpp
 *
 *  Created on: 03.06.2017
 *      Author: RENT
 */
#include "Map.hpp"
#include <algorithm>

namespace Game
{
struct FindTile
{
	int mX;
	int mY;
public:
	FindTile(int x, int y)
	:mX(x)
	,mY(y)
	{
	}
	bool operator()(const Tile& tile)
	{
		return tile.mX==mX && tile.mY==mY;
	}
};
Map::Map()
:mGame(0)
{
}
Map::Map(int width, int height)
:mGame(0)
{
	for(int x=0; x<width; x++)
	{
		for (int y=0; y<height; y++)
		{
			mTiles.push_back(Tile(x,y,false));
		}
	}
}
bool Map::isWall(int x, int y)
{
	auto it= std::find_if(mTiles.begin(), mTiles.end(), FindTile(x,y));
	return it->isWall;
}
void Map::setTile(int x, int y, bool wall)
{
	auto it= std::find_if(mTiles.begin(), mTiles.end(), FindTile(x, y));
	it->isWall=wall;
}
void Map::render(Game::Console& console)
{
	for(auto it=mTiles.begin(); it!=mTiles.end(); ++it)
	{
		if (it->isWall)
		{
			drawWall(console, it->mX, it->mY);
		}
		else
		{
			drawFloor(console, it->mX, it->mY);
		}
	}
}
void Map::drawWall(const Game::Console& console, int x, int y)
{
	console.drawChar(219, x, y, Game::FgColour::DARKGRAY, Game::BgColour::BLACK);
}
void Map::drawFloor(const Game::Console& console, int x, int y)
{
	console.drawChar(176, x, y, Game::FgColour::DARKBLUE, Game::BgColour::BLACK);//tu co� zmienic
}
}
