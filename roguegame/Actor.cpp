/*
 * Actor.cpp
 *
 *  Created on: 03.06.2017
 *      Author: RENT
 */
#include "Actor.hpp"
#include "Game.hpp"

namespace Game
{
void Actor:: render(Game::Console& console)
{
	console.drawChar(mSymbol,mX, mY, mColourF, mColourB);
}

Actor::Actor()
:mX(0)
,mY(0)
,mSymbol('$')
,mColourF(Game::FgColour::BLUE)
,mColourB(Game::BgColour::RED)
,mGame(0)
{
}
Actor::Actor(int x, int y, char symbol, Game::FgColour::Colour colourF, Game::BgColour::Colour colourB, RogueGame* game)
:mX(x)
,mY(y)
,mSymbol(symbol)
,mColourF(colourF)
,mColourB(colourB)
,mGame(game)
{
}

void Actor::setX(int x)
{
	mX=x;
}

int Actor::getX() const
{
	return mX;
}

int Actor::getY() const
{
	return mY;
}

void Actor:: setY(int y)
{
	mY=y;
}
void Actor:: setPos(int x, int y)
{
	setX(x);
	setY(y);
}
void Actor:: move(int x, int y)
{
	if (mGame->canMove(mX+x, mY+y))
	{
	mX+=x;
	mY+=y;
	}
}
bool Actor::checkCollision(Game::Actor actor)
{
		return actor.mX==this->mX && actor.mY==this->mY;
}
}
