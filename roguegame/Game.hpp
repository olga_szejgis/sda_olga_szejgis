/*
 * Game.hpp
 *
 *  Created on: 05.06.2017
 *      Author: RENT
 */

#ifndef GAME_HPP_
#define GAME_HPP_
#include <vector>
#include "Console.hpp"
#include "Enemy.hpp"
#include "Player.hpp"
#include "Map.hpp"

namespace Game
{
class RogueGame
{
Map mMap;
Player mPlayer;
std::vector<Enemy> mEnemies;
Console mConsole;

public:
RogueGame();
void init(const Game::Console& console);
void gameLoop(Key key);
bool canMove(int x, int y);
void draw();
void update(Key key);
};
}


#endif /* GAME_HPP_ */
