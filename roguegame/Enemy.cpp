/*
 * Enemy.cpp
 *
 *  Created on: 05.06.2017
 *      Author: RENT
 */
#include "Enemy.hpp"
#include "Game.hpp"

namespace Game
{
Enemy::Enemy()
:Actor()
{
}

Enemy::Enemy(int x, int y, char symbol, Game::FgColour::Colour colourF, Game::BgColour::Colour colourB, RogueGame* game)
:Actor(x, y, symbol, colourF, colourB, game)
{

}
void Enemy::moveEnemy()
{
	int move=rand()%4;
	int dX=0;
	int dY=0;

	switch(move)
	{
	case 0:
		dX=1;
		break;
	case 1:
		dX=1;
		break;
	case 2:
		dY=1;
		break;
	case 3:
		dY=1;
		break;
	}
	if (mGame->canMove(mX+dX, mY+dY))
		{
		mX+=dX;
		mY+=dY;
		}
}
void Enemy::updateEnemy()
{
	moveEnemy();
}
}

