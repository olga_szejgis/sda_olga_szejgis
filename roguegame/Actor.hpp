/*
 * Actor.hpp
 *
 *  Created on: 03.06.2017
 *      Author: RENT
 */

#ifndef ACTOR_HPP_
#define ACTOR_HPP_
#include "Console.hpp"

namespace Game
{
class RogueGame;
class Actor
{
protected:
	int mX;
	int mY;
	char mSymbol;
	Game::FgColour::Colour mColourF;
	Game::BgColour::Colour mColourB;
	RogueGame* mGame;

public:
	Actor();
	void render(Game::Console& console);
	Actor(int x, int y, char symbol, Game::FgColour::Colour colourF, Game::BgColour::Colour colourB, RogueGame* game);
	void setPos(int x, int y);
	void setX(int x);
	void setY(int y);
	void move(int x, int y);
	int getX() const;
	int getY() const;
	bool checkCollision(Game::Actor);
};
}

#endif /* ACTOR_HPP_ */
