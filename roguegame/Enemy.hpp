/*
 * Enemy.hpp
 *
 *  Created on: 05.06.2017
 *      Author: RENT
 */

#ifndef ENEMY_HPP_
#define ENEMY_HPP_
#include "Actor.hpp"

namespace Game
{
class Enemy: public Actor
{
public:
	Enemy();
	Enemy(int x, int y, char symbol, Game::FgColour::Colour colourF, Game::BgColour::Colour colourB, RogueGame* game);
	void updateEnemy();
	void moveEnemy();
};
}

#endif /* ENEMY_HPP_ */
