#include <iostream>
#include <cmath>

double
obliczDelte (double a, double b, double c)
{
  double delta = (b * b) - (4 * (a * c));
  return delta;
}

int
main ()
{

  double a, b, c, delta;

  std::cout << "Podaj a: " << std::endl;
  std::cin >> a;
  std::cout << "Podaj b: " << std::endl;
  std::cin >> b;
  std::cout << "Podaj c: " << std::endl;
  std::cin >> c;

  if (a == 0)
    {
      std::cerr << "Nie mo�na dzielic przez 0! " << std::endl;
    }
  else
    {
      delta = obliczDelte (a, b, c);

      if (delta < 0)
	{
	  std::cerr << "R�wnanie nie ma rozwiazan " << std::endl;
	}
      else if (delta == 0)
	{
	  std::cout << "Rozwiazanie: " << ((-b) + sqrt (delta)) / (2 * a);
	}
      else
	{
	  std::cout << "Rozwiazanie 1: " << ((-b) + sqrt (delta)) / (2 * a)
	      << std::endl;
	  std::cout << "Rozwiazanie 2: " << ((-b) - sqrt (delta)) / (2 * a)
	      << std::endl;
	}
    }
  return 0;
}
