//9. Napisz funkcj�, kt�ra stworzy wektor przechowuj�cy liczby od 1 do n.
//Nast�pnie utw�rz dwa wektory. Jeden, kt�rzy b�dzie przechowywa� tylko wielokrotno�ci 2, a drugi tylko wielokrotno�ci 3.
//Nast�pnie zwr�� vector przechowuj�cy tylko wielokrotno�ci 2 i 3. (remove_copy_if, set_intersection)

#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
using namespace std;


bool ifNotMultipleOf2(int n)
{
	//return (n%2)==1;
	if(0==n%2)
	{
		return false;
	}
	return true;
}

bool ifNotMultipleOf3(int n)
{
	//return (n%3)==1;
	if(0==n%3)
	{
		return false;
	}
	return true;
}

vector <int> multi (int n)
{
	vector<int> myVector;

	for (int i=1; i<=n; i++)
	{
		myVector.push_back(i);
	}

	vector<int>divBy2(myVector.size());
	vector<int>divBy3(myVector.size());

	vector<int>::iterator it;

	it= remove_copy_if(myVector.begin(), myVector.end(), divBy2.begin(), ifNotMultipleOf2);
	divBy2.resize(distance(divBy2.begin(), it));

	it=remove_copy_if(myVector.begin(), myVector.end(), divBy3.begin(), ifNotMultipleOf3);
	divBy3.resize(distance(divBy3.begin(), it));

	vector<int> divBy2and3(divBy2.size());

	it= set_intersection(divBy2.begin(), divBy2.end(), divBy3.begin(), divBy3.end(), divBy2and3.begin());
	divBy2and3.resize(distance(divBy2and3.begin(), it));


	return divBy2and3;

}
int main()
{

	vector<int> wektor=multi(15);
	for (vector<int>::iterator it=wektor.begin(); it!=wektor.end(); ++it)
				{
					cout << *it<< endl;
				}


	return 0;
}
