/*
 * main.cpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */
#include "kolor.hpp"
#include "kolo.hpp"
#include "kwadrat.hpp"

int main ()
{
Kolor::Kolor kolor(Kolor::niebieski);
std:: cout<< Kolor::convertToString(kolor)<<std::endl;
Kolo kolo ("kolo", 4.14, Kolor::niebieski);
Kwadrat kwadrat ("kwadrat", 14.5, Kolor::czerwony);

kwadrat.wypisz();
kolo.wypisz();
Figura*wskaznik;
wskaznik=&kwadrat;
wskaznik->wypisz();

	return 0;
}


