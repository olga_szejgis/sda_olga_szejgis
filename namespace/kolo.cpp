/*
 * kolo.cpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */
#include "kolo.hpp"

Kolo::Kolo()
:mNazwa("kolo")
,mPromien(6.5)
,mKolor(Kolor::czerwony)
{
}
Kolo::Kolo(std::string nazwa, float promien, Kolor::Kolor kolor)
:mNazwa(nazwa)
,mPromien(promien)
,mKolor(kolor)
{
}
void Kolo:: wypisz()
{
	std::cout << " Nazwa figury: "<< mNazwa<< " Promien: "<<mPromien << " Kolor: "<< Kolor::convertToString(mKolor)<< std::endl;
}
Kolo::~Kolo()
{

}


