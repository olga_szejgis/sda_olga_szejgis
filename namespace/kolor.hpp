/*
 * kolor.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef KOLOR_HPP_
#define KOLOR_HPP_
#include <iostream>

namespace Kolor
{
enum Kolor
{
	czerwony,
	zielony,
	niebieski
};
std::string convertToString(Kolor kolor);
}



#endif /* KOLOR_HPP_ */
