/*
 * kwadrat.cpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */
#include "kwadrat.hpp"

Kwadrat::Kwadrat()
:mNazwa("kwadrat")
,mDlugoscBoku(5.5)
,mKolor(Kolor::niebieski)
{
}
Kwadrat::Kwadrat(std::string nazwa, float dlugoscBoku, Kolor::Kolor kolor)
:mNazwa(nazwa)
,mDlugoscBoku(dlugoscBoku)
,mKolor(kolor)
{
}
void Kwadrat:: wypisz()
{

	std::cout << " Nazwa figury: "<< mNazwa<< " Dlugosc boku: "<<mDlugoscBoku << " Kolor: "<< Kolor::convertToString(mKolor)<< std::endl;
}
Kwadrat::~Kwadrat()
{

}
