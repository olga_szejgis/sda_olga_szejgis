/*
 * kwadrat.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef KWADRAT_HPP_
#define KWADRAT_HPP_
#include"figura.hpp"
#include "kolor.hpp"

class Kwadrat: public Figura
{
std::string mNazwa;
float mDlugoscBoku;
Kolor::Kolor mKolor;
public:
Kwadrat();
Kwadrat(std::string nazwa, float dlugoscBoku, Kolor::Kolor kolor);
void wypisz();
~Kwadrat();
};



#endif /* KWADRAT_HPP_ */
