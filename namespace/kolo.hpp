/*
 * kolo.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef KOLO_HPP_
#define KOLO_HPP_
#include"figura.hpp"
#include "kolor.hpp"

class Kolo: public Figura
{
	std::string mNazwa;
	float mPromien;
	Kolor::Kolor mKolor;
	public:
	Kolo();
	Kolo(std::string nazwa, float promien, Kolor::Kolor kolor);
	void wypisz();
	~Kolo();
};



#endif /* KOLO_HPP_ */
