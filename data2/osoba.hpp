/*
 * osoba.hpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#ifndef OSOBA_HPP_
#define OSOBA_HPP_
#include <iostream>
#include "data.hpp"

class Osoba
{
	std::string mImie;
	std::string mNazwisko;
	Data mData;
public:
	Osoba(std::string imie, std::string nazwisko, Data data);
};




#endif /* OSOBA_HPP_ */
