//============================================================================
// Name        : suma.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>


void wypelnij(double *tablica, int rozmiar)
{
	for (int i = 0; i < rozmiar; ++i)
	{
		std::cout << "Podaj " << i+1 << " liczbe: "<< std::endl;
		std::cin >> *(tablica+i);
	}
}

double srednia(double* x, int rozmiar)
{
	double suma = 0;
	for (int i = 0; i < rozmiar; ++i)
	{
		suma += *(x + i);
	}
	return suma / rozmiar;
}

int main()
{


	int rozmiar;
	std::cout << "Podaj ilosc liczb " << std::endl;
	std::cin >> rozmiar;

	double* tablica = new double[rozmiar];

	wypelnij(tablica, rozmiar);

	std::cout << "Srednia: " << srednia(tablica, rozmiar) << std::endl;

	return 0;
}
