//15. Do poprzedniego zadania dodaj numer sali oraz drugi plik z list� student�w i ich kursami na ktore sie zapisali:
//    Format: [imie_Nazwisko] [liczba_kursow] [kurs1] [kurs2] .... [kursN]
//    a Wyszukaj i wypisz wszystkie konfliktuj�ce kursy danego studenta
//    b) Wylicz ilu studentow uczeszcza na dany kurs (map)
//    c) Dodaj usuwanie zduplikowanych kursow

#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <cstdlib>
#include <map>

using namespace std;

struct Time
{
	int mHour;
	int mMinute;

	Time(int hour, int minute)
	:mHour(hour)
	,mMinute(minute)
	{
	}

	bool operator <( const Time second) const
	{
		if(this->mHour==second.mHour)
		{
			return this->mMinute<second.mMinute;
		}
		return this->mHour<second.mHour;
	}

	bool operator >( const Time second) const
	{
		if (this->mHour == second.mHour)
		{
			return this->mMinute > second.mMinute;
		}
		return this->mHour > second.mHour;
	}
	bool operator >=( const Time second) const
	{
		if (this->mHour == second.mHour)
		{
			return this->mMinute >= second.mMinute;
		}
		return this->mHour >= second.mHour;
	}
	bool operator <=( const Time second) const
	{
		if (this->mHour == second.mHour)
		{
			return this->mMinute <= second.mMinute;
		}
		return this->mHour <= second.mHour;
	}
	bool operator ==( const Time second) const
	{
		if (this->mHour == second.mHour)
		{
			return this->mMinute == second.mMinute;
		}
		return this->mHour == second.mHour;
	}
};

struct Course
{
	string mName;
	int mDay;
	Time mStart;
	Time mEnd;
	int mClassNumber;

	Course(string name, int day, int startHour, int startMinute, int endHour, int endMinute, int classNumber)
	:mName(name)
	,mDay(day)
	,mStart(startHour, startMinute)
	,mEnd(endHour, endMinute)
	,mClassNumber(classNumber)
	{
	}

	bool operator ==(const string second) const
	{
		if(this->mName==second)
		{
			return true;
		}
		return false;
	}
};

struct Student
{
	string mName;
	vector<Course> mCourses;

	Student(string name, vector<Course>courses)
	:mName(name)
	,mCourses(courses)
	{
	}

	bool ifCollision(Course first, Course second)
	{
		if (((first.mStart> second.mStart)&& (first.mStart< second.mEnd))
					|| ((first.mEnd > second.mStart)&& (first.mEnd < second.mEnd))
					|| ((first.mStart <= second.mStart)&& (first.mEnd>= second.mEnd))
					|| ((first.mStart == second.mStart)&& (first.mEnd == second.mEnd)))
		{
			return true;
		}
		return false;
	}

	void checkCollision()
	{
		for(vector<Course>::iterator it=mCourses.begin(); it!=mCourses.end(); advance(it,1))
		{
			for(vector<Course>::iterator it2=it+1; it2!=mCourses.end(); advance(it2,1))
			{
				if(it->mDay==it2->mDay)
				{
					if(ifCollision(*it, *it2))
					{
						cout<<"Collision!"<<endl;
						cout<<it->mName<<" "<<it->mStart.mHour<<" "<<it->mStart.mMinute<<" "<<it->mEnd.mHour<<" "<<it->mEnd.mMinute<<" "<<it->mClassNumber<<endl;
						cout<<it2->mName<<" "<<it2->mStart.mHour<<" "<<it2->mStart.mMinute<<" "<<it2->mEnd.mHour<<" "<<it2->mEnd.mMinute<<" "<<it2->mClassNumber<<endl;
					}
				}
			}
		}
	}
};


bool sortCourse(Course first, Course second)
{
	if(first.mDay==second.mDay)
	{
		return first.mStart<second.mStart; // trzeba nadpisac operator porownania dla Time
	}
	else
	{
		return first.mDay<second.mDay;
	}
}

void setStart(int start, int& startHour, int& startMinute)
{
	string tmp;
	string str;

	sprintf((char*) tmp.c_str(), "%d", start);
	str = tmp.c_str();

	if(str.length()==4)
	{
		startHour= (str[1]-'0')+((str[0]-'0')*10);
		startMinute= (str[3]-'0')+((str[2]-'0')*10);
	}
	else
	{
		startHour= str[0]-'0';
		startMinute= (str[2]-'0')+((str[1]-'0')*10);
	}
}

void setEnd(int end, int& endHour, int& endMinute)
{
	string tmp;
	string str;

	sprintf((char*) tmp.c_str(), "%d", end);
	str = tmp.c_str();

	endHour= (str[1]-'0')+((str[0]-'0')*10);
	endMinute= (str[3]-'0')+((str[2]-'0')*10);
}

void countStudentsAtCourse(map<string, int> &mapToCheck, vector<Student>studentsList)
{
    map<string, int>::iterator itMap;
	for(vector<Student>::iterator it=studentsList.begin(); it!=studentsList.end(); advance(it,1))
    {
        for(vector<Course>::iterator it2= it->mCourses.begin(); it2!= it->mCourses.end(); advance(it2,1))
        {
        	itMap=mapToCheck.find(it2->mName);
        	itMap->second++;
        }
    }
}

void showStudentsAtCurse(map<string, int> mapToCheck)
{
	cout<<"Studenci na kursie "<< endl;
	for (map<string, int>::iterator it= mapToCheck.begin(); it!=mapToCheck.end(); ++it)
	{
		cout<< it-> first<< " "<< it->second<<endl;
	}
}

int main()
{
	vector<Course> courses;

	ifstream file;
	file.open("courses.txt");

    if (!file.good())
    {
        cerr<< "Error reding file"<< endl;
        return 1;
    }

    string name;
    int day;
    int start;
    int end;
    int startHour;
    int startMinute;
    int endHour;
    int endMinute;
    int classNumber;

    while (!file.eof())
	{
		file >> name >> day >> start >> end >> classNumber;
		setStart(start, startHour, startMinute);
		setEnd(end, endHour, endMinute);
		courses.push_back(Course(name, day, startHour, startMinute, endHour, endMinute, classNumber));
	}

    file.close();


    sort(courses.begin(),courses.end(), sortCourse);

    vector<Student>students;

    ifstream studentsList;
    studentsList.open("students.txt");

    if (!studentsList.good())
     {
         cerr<< "Error reading file"<< endl;
         return 1;
     }

    string studentsName;
    int coursesNumber;
    vector<Course> coursesNames;
    vector<string> coursesNamesString;
    string currentCName;
    vector<Course>::iterator it2;

    while(!studentsList.eof())
    {
    	coursesNames.clear();
    	coursesNamesString.clear();
    	studentsList >> studentsName >> coursesNumber;
    	for(int i=0; i<coursesNumber; ++i)
    	{
    		studentsList >> currentCName;
    		coursesNamesString.push_back(currentCName);
    	}
    	for(vector<string>::iterator it=coursesNamesString.begin(); it!=coursesNamesString.end(); advance(it,1))
    	{
    		it2 = find(courses.begin(), courses.end(), *it);
    		coursesNames.push_back(*it2);
    	}

    	students.push_back(Student(studentsName,coursesNames));
    }

    studentsList.close();

    for(vector<Student>::iterator it=students.begin(); it!=students.end(); advance(it, 1))
	{
    	cout<<it->mName<<" ";
    	for(vector<Course>::iterator it2=it->mCourses.begin(); it2!=it->mCourses.end(); advance(it2, 1))
    	{
    		cout<<it2->mName<<" ";
    	}
    	cout<<endl;
	}

    for(vector<Student>::iterator it=students.begin(); it!=students.end(); advance(it,1))
    {
    	it->checkCollision();
    }

    map<string, int> courseMap;

    for (vector<Course>::iterator it=courses.begin();it!=courses.end(); advance(it,1))
    {
    	courseMap.insert(pair<string, int>(it->mName,0));
    }

    countStudentsAtCourse(courseMap,students);
    showStudentsAtCurse(courseMap);

	return 0;
}
