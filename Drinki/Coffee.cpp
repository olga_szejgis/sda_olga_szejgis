/*
 * Coffee.cpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */
#include "Coffee.hpp"

Coffee::Coffee()
:mCaffeine(0)
{
}
Coffee::Coffee(int amount, int caffeine)
:Liquid(amount)
,mCaffeine(caffeine)
{
}
void Coffee:: add(int amount)
{
	mAmount+=amount;
}
void Coffee:: remove(int amount)
{
	mAmount=(mAmount > amount) ? mAmount-amount :0;
}
void Coffee::removeAll()
{
	mAmount=0;
}
Coffee::~Coffee()
{

}

