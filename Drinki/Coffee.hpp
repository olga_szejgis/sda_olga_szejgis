/*
 * Coffee.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef COFFEE_HPP_
#define COFFEE_HPP_
#include "Liquid.hpp"

class Coffee: public Liquid
{
protected:
	int mCaffeine;

public:
	Coffee();
	Coffee(int amount, int caffeine);
	virtual void add(int amount);
	virtual void remove(int amount);
	virtual void removeAll ();
	virtual ~Coffee();
};



#endif /* COFFEE_HPP_ */
