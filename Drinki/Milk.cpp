#include "Milk.hpp"


Milk::Milk()
:mFat(0)
{
}
Milk::Milk(int amount, float fat)
:Liquid(amount)
,mFat(fat)
{
}
void Milk::add(int amount)
{
	mAmount+=amount;
}
void Milk::remove(int amount)
{
	mAmount=(mAmount > amount) ? mAmount-amount :0;
}
void Milk::removeAll()
{
	mAmount=0;
}
Milk::~Milk()
{
}


