#include "Rum.hpp"

Rum::Rum()
:mType(Dark)
{
}
Rum::Rum(int amount, Colour type)
:Liquid(amount)
,mType(type)
{
}
void Rum::add(int amount)
{
	mAmount+=amount;
}
void Rum::remove(int amount)
{
	mAmount=(mAmount > amount) ? mAmount-amount :0;
}
void Rum::removeAll()
{
	mAmount=0;
}
Rum::~Rum()
{

}


