/*
 * Cup.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef CUP_HPP_
#define CUP_HPP_
#include "Liquid.hpp"
#include <cstdlib>

class Cup
{
Liquid** mContent;
size_t mSize;
void resize()
{
	if (mSize==0)
	{
		mContent = new Liquid*[++mSize];
	}
	else
	{
		Liquid** tmp = new Liquid* [++mSize];
		for (size_t i=0; i<mSize-1;i++)
		{
			tmp[i]= mContent[i];
		}
		delete [] mContent;
		mContent=tmp;
	}
}
void clear()
{
	for (size_t i=0; i<mSize-1;i++)
	{
		delete mContent[i];
	}
	delete [] mContent;
	mContent=0;
	mSize=0;
}
int calculateAmount()
{
	int liquidAmount=0;
	if (mSize==0)
			{
				//do nothing
			}
			else
			{
				for (size_t i=0; i<mSize-1;i++)
				{
					liquidAmount+= mContent[i]->getAmount();
				}
			}
	return liquidAmount;
}

public:

	Cup()
:mContent(0)
,mSize(0)
{
}
	void add (Liquid* liquid)
	{
		resize();
		mContent[mSize-1]=liquid;
	}

	void takeSip(int amount)
	{
		int liquidAmount=calculateAmount();
		if (liquidAmount>0 && amount>0)
		{
			if (liquidAmount<=amount)
			{
				spill(); // teoretycznie powinna byc osobna funkcja ale podobna
			}
			else
			{
				for (size_t i=0; i<mSize-1; i++)
				{
					float liquidRatio=(float) mContent[i]->getAmount()/(float)liquidAmount;
					mContent[i]->remove(liquidRatio*amount);
				}
			}
		}
		else
		{
			//do nothing
		}
	}

	void spill()
	{
		for (size_t i=0; i<mSize-1;i++)
		{
			mContent[i]->removeAll();
		}
		clear();
	}
	void wypisz()
	{

	}

};



#endif /* CUP_HPP_ */
