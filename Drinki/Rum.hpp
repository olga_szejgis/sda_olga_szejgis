/*
 * Rum.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef RUM_HPP_
#define RUM_HPP_
#include "Liquid.hpp"

class Rum: public Liquid
{
enum Colour
{
	Light,
	Dark
};

Colour mType;

public:
	Rum();
	Rum(int amount, Colour type);
	void add(int amount);
	void remove(int amount);
	void removeAll ();
	~Rum();
};



#endif /* RUM_HPP_ */
