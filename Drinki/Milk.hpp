/*
 * Milk.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef MILK_HPP_
#define MILK_HPP_
#include "Liquid.hpp"

class Milk: public Liquid
{
	float mFat;

public:
	Milk();
	Milk(int amount, float fat);
	void add(int amount);
	void remove(int amount);
	void removeAll();
	~Milk();
};


#endif /* MILK_HPP_ */
