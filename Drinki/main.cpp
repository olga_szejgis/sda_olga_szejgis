/*
 * main.cpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */
#include "Liquid.hpp"
#include "Cup.hpp"
#include "Coffee.hpp"
#include "Milk.hpp"
#include "Rum.hpp"
#include "Decaff.hpp"
#include "Espresso.hpp"

int main ()
{
Cup kubekKawyZMlekiem;

kubekKawyZMlekiem.add(new Decaff(250));
kubekKawyZMlekiem.add(new Milk(100, 2.0));

	return 0;
}

