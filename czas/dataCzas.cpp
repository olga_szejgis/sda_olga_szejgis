/*
 * dataCzas.cpp
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */
#include "dataCzas.hpp"

DataCzas::DataCzas()
:Data(1, 1, 1900)
,Czas(0, 0, 0)
{

}
DataCzas::DataCzas(unsigned int dzien, unsigned int miesiac, unsigned int rok, unsigned int godziny, unsigned int minuty, unsigned int sekundy)
{
	setDzien(dzien);
	setMiesiac(miesiac);
	setRok(rok);
	setGodziny(godziny);
	setMinuty(minuty);
	setSekundy(sekundy);
}

DataCzas::DataCzas(Data d, Czas c)
:Data(d.getDzien(), d.getMiesiac(), d.getRok())
,Czas(c.getGodziny(), c.getMinuty(), c.getSekundy())
{

}
