#ifndef DATA_HPP_
#define DATA_HPP_
#include <iostream>

class Data
{
protected:
	 unsigned int mDzien;
	 unsigned int mMiesiac;
	 unsigned int mRok;

	public:
	Data(unsigned int dzien, unsigned int miesiac, unsigned int rok);
	Data();
	unsigned int getDzien() const;
	void setDzien(unsigned int dzien);
	unsigned int getMiesiac() const;
	void setMiesiac(unsigned int miesiac);
	unsigned int getRok() const;
	void setRok(unsigned int rok);
	void wypisz ();
	void przesunDzien(int ileDni);
	void przesunMiesiac(int ileMiesiecy);
	void przesunRok(int ileLat);
};



#endif /* DATA_HPP_ */
