/*
 * czas.hpp
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef CZAS_HPP_
#define CZAS_HPP_
#include <iostream>

class Czas
{
protected:

unsigned int mGodziny;
unsigned int mMinuty;
unsigned int mSekundy;

public:

	Czas();
	Czas (unsigned int godziny, unsigned int minuty, unsigned int sekundy);
	unsigned int getGodziny() const;
	void setGodziny(unsigned int godziny);
	unsigned int getMinuty() const;
	void setMinuty(unsigned int minuty);
	unsigned int getSekundy() const;
	void setSekundy(unsigned int sekundy);
	void wypisz();
	void przesunGodziny(int ileGodzin);
	void przesunMinuty(int ileMinut);
	void przesunSekundy(int ileSekund);
};



#endif /* CZAS_HPP_ */
