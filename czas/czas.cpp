#include "czas.hpp"

Czas:: Czas()
:mGodziny(0)
,mMinuty(0)
,mSekundy(0)
{
}

Czas::Czas (unsigned int godziny, unsigned int minuty, unsigned int sekundy)
{
setGodziny(godziny);
setMinuty(minuty);
setSekundy(sekundy);
}

unsigned int Czas::getGodziny() const
{
	return mGodziny;
}

void Czas::setGodziny(unsigned int godziny)
{
	mGodziny = godziny;
}

unsigned int Czas::getMinuty() const
{
	return mMinuty;
}

void Czas::setMinuty(unsigned int minuty)
{
	mMinuty = minuty;
}

unsigned int Czas::getSekundy() const
{
	return mSekundy;
}

void Czas::setSekundy(unsigned int sekundy)
{
	mSekundy = sekundy;
}

void Czas:: wypisz()
{
std::cout << mGodziny << ": "<< mMinuty << ": "<< mSekundy<< std::endl;
}
void Czas::przesunGodziny(int ileGodzin)
{
mGodziny+=ileGodzin;
if (mGodziny>24)
{
	mGodziny=24;
}

}

void Czas::przesunMinuty(int ileMinut)
{
	if (ileMinut > 59)
	{
		przesunGodziny(ileMinut / 60);
		int resztaMinut = ileMinut % 60;
		if ((mMinuty + resztaMinut) > 60)
		{
			mGodziny++;
			mMinuty = (mMinuty + resztaMinut) % 60;
		}
		else if ((mMinuty + resztaMinut) <= 60)
		{
			mMinuty += resztaMinut;
		}
	}
	else if ((mMinuty + ileMinut) > 60)
	{
		mGodziny++;
		mMinuty = (mMinuty + ileMinut) % 60;
	}
	else if ((mMinuty + ileMinut) <= 60)
	{
		mMinuty += ileMinut;
	}
}
void Czas::przesunSekundy(int ileSekund)
{
	if (ileSekund > 59)
	{
		przesunMinuty(ileSekund / 60);
		int resztaSekund = ileSekund % 60;
		if ((mSekundy + resztaSekund) > 60)
		{
			mMinuty++;
			mSekundy = (mSekundy + resztaSekund) % 60;
		}
		else if ((mSekundy + resztaSekund) <= 60)
		{
			mSekundy += resztaSekund;
		}
	}
	else if ((mSekundy + ileSekund) > 60)
	{
		mMinuty++;
		mSekundy = (mSekundy + ileSekund) % 60;
	}
	else if ((mSekundy + ileSekund) <= 60)
	{
		mSekundy += ileSekund;
	}
}


