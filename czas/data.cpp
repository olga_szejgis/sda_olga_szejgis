#include "data.hpp"

Data::Data ()
:mDzien(1)
,mMiesiac(1)
,mRok(1900)
{
}

Data::Data (unsigned int dzien, unsigned int miesiac, unsigned int rok)
{
	 setDzien(dzien);
	 setMiesiac(miesiac);
	 setRok(rok);
}

unsigned int Data::getDzien() const
{
	return mDzien;
}

void Data::setDzien(unsigned int dzien)
{
	 if (dzien<1)
	        mDzien=1;
	    else if (dzien>31)
	        mDzien=31;
	    else
	    	mDzien = dzien;
}

unsigned int Data::getMiesiac() const
{
	return mMiesiac;
}

void Data::setMiesiac(unsigned int miesiac)
{
	 if (miesiac<1)
	            mMiesiac=1;
	        else if (miesiac>12)
	            mMiesiac=12;
	        else
	        	mMiesiac = miesiac;
}

unsigned int Data::getRok() const
{
	return mRok;
}

void Data::setRok(unsigned int rok)
{
	  if (rok<1900)
	            mRok=1900;
	        else
	        	mRok = rok;
}

void Data:: wypisz ()
{
	std::cout<< "Data: "<< mDzien<< "."<< mMiesiac << "."<<mRok<< std::endl;
}

void Data::przesunDzien(int ileDni)
    {
        if(ileDni>31)
        {
            przesunMiesiac(ileDni/31);
            int resztaDni = ileDni%31;
            if((mDzien+resztaDni)>31)
            {
                mMiesiac++;
                mDzien = (mDzien+resztaDni)%31;
            }
            else if((mDzien+resztaDni)<=31)
            {
                mDzien+=resztaDni;
            }
        }
        else if((mDzien+ileDni)>31)
        {
            mMiesiac++;
            mDzien = (mDzien+ileDni)%31;
        }
        else if((mDzien+ileDni)<=31)
        {
            mDzien+=ileDni;
        }
    }

    void Data::przesunMiesiac(int ileMiesiecy)
    {
        if(ileMiesiecy>12)
        {
            przesunRok(ileMiesiecy/12);
            int resztaMiesiecy = ileMiesiecy%12;
            if((mMiesiac+resztaMiesiecy)>12)
            {
                    mRok++;
                    mMiesiac = (mMiesiac+resztaMiesiecy)%12;
            }
            else if((mMiesiac+resztaMiesiecy)<=12)
            {
                    mMiesiac+=resztaMiesiecy;
            }
        }
        else if((mMiesiac+ileMiesiecy)>12)
        {
            mRok++;
            mMiesiac = (mMiesiac+ileMiesiecy)%12;
        }
        else if ((mMiesiac+ileMiesiecy)<=12)
        {
            mMiesiac+=ileMiesiecy;
        }
    }

    void Data::przesunRok(int ileLat)
    {
        mRok+=ileLat;
    }
