/*
 * dataCzas.hpp
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef DATACZAS_HPP_
#define DATACZAS_HPP_
#include "czas.hpp"
#include "data.hpp"


class DataCzas: public Data, Czas
{

public:
	DataCzas();
	DataCzas(unsigned int dzien, unsigned int miesiac, unsigned int rok, unsigned int godziny, unsigned int minuty, unsigned int sekundy);
	DataCzas(Data d, Czas c);
};



#endif /* DATACZAS_HPP_ */
