/*
 * SerZolty.hpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#ifndef SERZOLTY_HPP_
#define SERZOLTY_HPP_
#include <iostream>
#include "Ser.hpp"

class SerZolty: public Ser
{
	int mWiek;

public:
	void podajWiek();
	SerZolty(double cena, int wiek);
	SerZolty();
};



#endif /* SERZOLTY_HPP_ */
