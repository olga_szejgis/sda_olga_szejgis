#include "Ser.hpp"
#include "SerZolty.hpp"
#include "SerBialy.hpp"

void sertuj(Ser** tab, int rozmiar)
{
	SerBialy** bialaTab=new SerBialy*[rozmiar];
	SerZolty** zoltaTab=new SerZolty*[rozmiar];
	int ileBialych=0;
	int ileZoltych=0;

	SerBialy* bialyTmp=0;
	SerZolty* zoltyTmp=0;

	for(int i=0; i<rozmiar; i++)
	{
		//rodziela wszystkie sery do odpwiendich tablic
		bialyTmp=dynamic_cast<SerBialy*>(tab[i]);

		if (bialyTmp)
		{
			bialaTab[ileBialych]=bialyTmp;
			ileBialych++;
			continue;
		}
		zoltyTmp=dynamic_cast<SerZolty*>(tab[i]);
		if (zoltyTmp)
		{
			zoltaTab[ileZoltych]=zoltyTmp;
			ileZoltych++;
		}
	}

	for(int i=0; i<ileBialych;i++)
	{
//		wypisujemy cene i rodzaj serow bialych
		std::cout<< "Bialy "<< std::endl;
		bialaTab[i]->podajCene();
		bialaTab[i]->podajRodzaj();
		std::cout<< std:: endl;
	}

	for(int i=0; i<ileZoltych;i++)
	{
//		wypisujemy cene i wiek serow zoltych
		std::cout<< "Zolty "<< std::endl;
		zoltaTab[i]->podajCene();
		zoltaTab[i]->podajWiek();
		std::cout<< std:: endl;
	}
}


int main ()
{
	Ser* sery[8];

		sery[0] = new SerBialy(10, SerBialy::Chudy);
		sery[1] = new SerZolty(301, 23);
		sery[2] = new SerZolty(10, 1);
		sery[3] = new SerBialy(8, SerBialy::Tlusty);
		sery[4] = new SerBialy(9, SerBialy::Tlusty);
		sery[5] = new SerBialy(15, SerBialy::Poltlusty);
		sery[6] = new SerBialy(13, SerBialy::Chudy);
		sery[7] = new SerZolty(2000, 33);

		sertuj(sery, 8);

	return 0;
}


