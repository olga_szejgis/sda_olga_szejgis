/*
 * Ser.hpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#ifndef SER_HPP_
#define SER_HPP_
#include<iostream>

class Ser
{
	float mCena;

public:

	Ser()
	:mCena(0)
	{
	}
	Ser(float cena)
	:mCena(cena)
	{
	}
	void podajCene()
	{
		std::cout << "Cena sera:" << mCena<< std::endl;
	}
	virtual ~Ser()
	{
	}
};



#endif /* SER_HPP_ */
