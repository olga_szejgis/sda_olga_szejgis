/*
 * SerBialy.hpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#ifndef SERBIALY_HPP_
#define SERBIALY_HPP_
#include "Ser.hpp"
#include <iostream>

class SerBialy: public Ser
{
public:
	enum Rodzaj
	{
		Chudy,
		Poltlusty,
		Tlusty
	};
private:

	Rodzaj mRodzaj;
	std::string wypiszRodzaj();
public:
	void podajRodzaj();
	SerBialy(double cena, Rodzaj rodzaj);
	SerBialy();
};



#endif /* SERBIALY_HPP_ */
