/*
 * StringCalcTest.cpp
 *
 *  Created on: 20.04.2017
 *      Author: RENT
 */


#include "gtest/gtest.h"
#include "StringCalculator.hpp"


int main(int argc, char **argv) {

      ::testing::InitGoogleTest(&argc, argv);

      return RUN_ALL_TESTS();

}

TEST(StringCalculator, EmptyString)
{
	StringCalculator c;
	EXPECT_EQ(0, c.Add(""));
}

TEST(StringCalculator, OneNumber)
{
	StringCalculator c;
	EXPECT_EQ(1, c.Add("1"));
}

TEST(StringCalculator, TwoNumbers)
{
	StringCalculator c;
	EXPECT_EQ(3, c.Add("1,2"));
	EXPECT_EQ(33, c.Add("12,21"));
	EXPECT_EQ(153, c.Add("141,12"));
}

