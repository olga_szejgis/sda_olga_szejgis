/*
 * Polonez.hpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#ifndef POLONEZ_HPP_
#define POLONEZ_HPP_
#include "samochod.hpp"

class cPolonez: public cSamochod
{
	float mPojemnosc;
	std::string mMarka;
	std::string mKolor;

public:
cPolonez(float pojemnosc, std::string marka, std::string kolor);
void kolor();
void marka();
void pojemnosc();
~cPolonez ()
{
	std::cout << "Destruktor poloneza " << std:: endl;
}
};




#endif /* POLONEZ_HPP_ */
