/*
 * Garbus.hpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#ifndef GARBUS_HPP_
#define GARBUS_HPP_
#include "samochod.hpp"

class cGarbus: public cSamochod
{
	float mPojemnosc;
	std::string mMarka;
	std::string mKolor;

public:
cGarbus(float pojemnosc, std::string marka, std::string kolor);
void kolor();
void marka();
void pojemnosc();
~cGarbus ()
{
	std::cout << "Destruktor garbusa" << std:: endl;
}

};





#endif /* GARBUS_HPP_ */
