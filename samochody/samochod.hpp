/*
 * samochod.hpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#ifndef SAMOCHOD_HPP_
#define SAMOCHOD_HPP_

#include <iostream>

class cSamochod
{
public:
void virtual kolor()=0;
void virtual marka()=0;
void virtual pojemnosc()=0;
virtual ~cSamochod()
{
	std::cout << "~Samochod" << std::endl;
}

};



#endif /* SAMOCHOD_HPP_ */
