/*
 * samochodyMain.cpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#include "Polonez.hpp"
#include "Garbus.hpp"
#include <iostream>

int main()
{
	cSamochod* wozy [2];
	wozy[0]= new cPolonez(1.3, "Szerszen ", "zolty");
	wozy[1]= new cGarbus(1.4, "Garbus ", "zolty");

	for (int i =0; i<2; i++)
	{
		wozy[i]->kolor();
		wozy[i]->marka();
		wozy[i]->pojemnosc();
	}

	for (int i =0; i<2; i++)
		{
			delete wozy[i];
		}

	return 0;

}


