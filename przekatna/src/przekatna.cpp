//============================================================================
// Name        : przekatna.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main()
{
	const int stala[5][5] = { { 1, 2, 3, 4, 5 }, { 6, 7, 8, 9, 10 }, { 11, 12, 13, 14, 15 }, { 16, 17, 18, 19, 20 }, { 21, 22, 23, 24, 25 } };

	int wynikPonizej=0;
	int wynikPowyzej=0;

	for (int i=0; i<5; i++)
	{
		for (int k=0; k<5; k++)
		{
			if (k>i)
			{
				wynikPowyzej+=stala[i][k];
			}
			if (k<i)
			{
				wynikPonizej+=stala[i][k];
			}
		}
	}
	cout<< wynikPonizej << endl;
	cout<< wynikPowyzej << endl;

	return 0;
}
