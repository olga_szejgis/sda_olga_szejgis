/*
 * student.hpp
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */

#ifndef STUDENT_HPP_
#define STUDENT_HPP_
#include <iostream>
#include "osoba.hpp"

class Student: public virtual Osoba
{
protected:
	int mIndeks;
	std::string mKierunek;
public:
	Student (int indeks, std::string kierunek, long pesel);
	Student (int indeks, std::string kierunek);
	void podajNumer();
	void virtual uczSie ();
	virtual ~Student ();
};



#endif /* STUDENT_HPP_ */
