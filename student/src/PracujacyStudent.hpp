/*
 * PracujacyStudent.hpp
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */

#ifndef PRACUJACYSTUDENT_HPP_
#define PRACUJACYSTUDENT_HPP_
#include <iostream>
#include "pracownik.hpp"
#include "student.hpp"


class PracujacyStudent: public Student, public Pracownik
{
public:

PracujacyStudent(int indeks, std::string kierunek, float pensja, std:: string zawod, long pesel)
:Osoba (pesel)
,Student (indeks, kierunek, pesel)
,Pracownik (pensja, zawod, pesel)
{
}
void virtual uczSie ()
	{
	std:: cout << "Obijam sie i nie ucze "<< std:: endl;
	}
void virtual pracuj ()

	{
	std::cout << "Pracuje tylko jak szef patrzy" << std:: endl;
	}
};



#endif /* PRACUJACYSTUDENT_HPP_ */
