/*
 * pracownik.hpp
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */

#ifndef PRACOWNIK_HPP_
#define PRACOWNIK_HPP_
#include <iostream>
#include "osoba.hpp"

class Pracownik: public virtual Osoba
{
protected:
	float mPensja;
	std::string mZawod;

public:
	Pracownik(float pensja, std::string zawod, long pesel);
	Pracownik(float pensja, std::string zawod);
	void virtual pracuj();
	virtual ~Pracownik ();
};




#endif /* PRACOWNIK_HPP_ */
