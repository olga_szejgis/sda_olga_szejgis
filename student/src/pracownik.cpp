/*
 * pracownik.cpp
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */

#include "pracownik.hpp"

Pracownik::Pracownik(float pensja, std::string zawod, long pesel)
:Osoba (pesel)
,mPensja(pensja)
,mZawod(zawod)
 {
 }

Pracownik::Pracownik(float pensja, std::string zawod)
:mPensja(pensja)
,mZawod(zawod)
 {
 }
void Pracownik::pracuj ()
{
	std::cout << "Pracuje ciezko " << std:: endl;
}

Pracownik::~Pracownik ()
{
	std::cout << "Destruktor pracownika " << std:: endl;
}
