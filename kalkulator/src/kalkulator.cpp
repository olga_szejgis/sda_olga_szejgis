#include <iostream>
using namespace std;

int
main ()
{
  float liczba1, liczba2;
  int wybor;

  cout << "MENU KALKULATORA " << endl;
  cout << ".................." << endl;
  cout << "1. DODAWANIE " << endl;
  cout << "2. ODEJMOWANIE " << endl;
  cout << "3. MNOZENIE " << endl;
  cout << "4. DZIELENIE " << endl;
  cout << " Podaj 1 liczbe " << endl;
  cin >> liczba1;
  cout << " Podaj 2 liczbe " << endl;
  cin >> liczba2;
  cout << " Jakie dzialanie chcesz wykonac? " << endl;
  cin >> wybor;
  switch (wybor)
  {
    case 1:
      {
	cout << "SUMA= " << liczba1 + liczba2 << endl;
      }
	break;
    case 2:
      {
	cout << "ROZNICA= " << liczba1 - liczba2 << endl;
      }
	break;
    case 3:
      {
	cout << "ILOCZYN= " << liczba1 * liczba2 << endl;
      }
	break;
    case 4:
      {
	cout << "ILORAZ= " << liczba1 / liczba2 << endl;
      }
	break;
    default:
      {
	cout << "Nie ma takiej opcji. Podaj liczbe od 1 do 4! " << endl;
      }
  }
  return 0;
}
