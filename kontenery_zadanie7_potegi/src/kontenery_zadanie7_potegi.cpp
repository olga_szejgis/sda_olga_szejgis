//7. Napisz funkcj�, kt�ra stworzy wektor przechowuj�cy pot�gi kwadratowe z podanego zakresu np od. 3 do 10 (for_each)
//8. Zmie� poprzednie zadanie tak aby zwraca�o sum� kwadrat�w (accumulate)

#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <numeric>
using namespace std;

void power (int& x)
{
	x=x*x;
}

vector <int> powerVector(int first, int last)
{
	vector<int> myVector;

	for(int i=first; i<=last; i++)
	{
		myVector.push_back(i);
	}
	 for_each(myVector.begin(), myVector.end(), power);

	 return myVector;
}

int accumulatePower(int first, int last)
{
	vector<int> toSum= powerVector(first, last);
	return accumulate(toSum.begin(), toSum.end(),0);
}

int main()
{
	vector<int> powers= powerVector(3, 7);

	for (vector<int>::iterator it=powers.begin(); it!=powers.end(); ++it)
			{
				cout << *it<< endl;
			}

	cout<< endl;
	cout<< accumulatePower(2,3);

	return 0;
}
