#include<iostream>
#include"Punkt.hpp"
#include"Zbior.hpp"

using namespace std;

int main()
{
	Punkt p1(2,3);
	Punkt p2=p1+1.1f;
	Punkt p3=2.5f+p2;

	Zbior zbior;
	zbior + p1;
	zbior + p2;
	zbior + p3;
	zbior.wypisz();

	Zbior zbiornik;
	zbiornik + Punkt(33,33);
	zbiornik + Punkt(11,11);
	zbior + zbiornik;
	zbior.wypisz();


	return 0;
}
