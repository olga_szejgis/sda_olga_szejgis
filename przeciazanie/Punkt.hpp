#ifndef PUNKT_HPP_
#define PUNKT_HPP_

class Punkt
{
	float mX;
	float mY;

public:
	Punkt(float x, float y)
	:mX(x)
	,mY(y)
	{
	}

	Punkt()
	:mX(0)
	,mY(0)
	{
	}

	Punkt(const Punkt &ptr)
	:mX(ptr.getX())
	,mY(ptr.getY())
	{
	}

	Punkt operator +( const float plus) const
	{
		Punkt wynik(0,0);
		wynik.setX(mX+plus);
		wynik.setY(mY+plus);
		return wynik;
	}

	Punkt operator +( const Punkt plus) const
	{
		Punkt wynik(0,0);
		wynik.setX(mX+plus.getX());
		wynik.setY(mY+plus.getY());
		return wynik;
	}

	Punkt operator *( const float dupa) const
	{
		Punkt wynik(0,0);
		wynik.setX(mX*dupa);
		wynik.setY(mY*dupa);
		return wynik;
	}


	float getX() const
	{
		return mX;
	}

	void setX(float x)
	{
		mX = x;
	}

	float getY() const
	{
		return mY;
	}

	void setY(float y)
	{
		mY = y;
	}

	void wypisz()
	{
		//std::cout<<"mX="<<getX()<<" mY="<<getY()<<std::endl;
		std::cout<<"["<<this->getX()<<", "<<this->getY()<<"]";
	}

	Punkt& operator++ ()
		{
		this->setX(mX++);
		this->setY(mY++);

		return *this;
		}

};

Punkt operator *( const float multi, const Punkt &punkt )
{
	return punkt*multi;
}

Punkt operator +( const float plus, const Punkt &punkt )
{
	return punkt+plus;
}



#endif /* PUNKT_HPP_ */

