
//Napisz funkcj�, kt�ra sprawdzi czy podany string jest palindromem (reverse i transform)

#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <list>
using namespace std;

//bool ifPalindrom(string toReverse)
//{
//	vector<char> myVector;
//	for (unsigned int i=0; i<toReverse.size(); i++)
//		 	{
//		 		myVector.push_back(toReverse[i]);
//		 	}
//	reverse(myVector.begin(), myVector.end());
//
//}

bool ifPalindrome(string toCheck)
{
	return equal(toCheck.begin(), toCheck.begin() + toCheck.size()/2, toCheck.rbegin());
}

int main()
{
	cout << ifPalindrome("kaczuszka")<< endl;
	cout <<ifPalindrome ("kajak")<<endl;

	return 0;
}
