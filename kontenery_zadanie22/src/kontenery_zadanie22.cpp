//22. Klasa NIEabstrakcyjna bazowa Animal (przechowuj�ca inta - numer seryjny) + dwie klasy pochodne Dog, Bird przeci�zaj�ce wirtualna metod� whoAreYou() -> wypisujaca nazw� klasy.
//    Stworzy� kontener na klas� Animal i wype�ni� go losowo 200 obiektami Dog lub Bird nadaj�c im kolejne numery seryjne. Nast�pnie wywo�a� na ka�dym z element�w metod� whoAreYou.
//    Rodzieli� Dog od Bird na dwia osobne kontenery i ponownie wywolac metod� whoAreYou().

#include <iostream>
#include<cstdlib>
#include<ctime>
#include <vector>
#include<algorithm>
using namespace std;

class Animal
{
protected:
	int mSerialNumber;

public:
	virtual void whoAreYou()
	{
		cout<<"Animal"<< endl;
	}

	Animal()
	:mSerialNumber(0)
	{
	}
	Animal(int serialNumber)
	:mSerialNumber(serialNumber)
	{
	}
	virtual~Animal()
	{
	}
};

class Dog: public Animal
	{
	public:
		void whoAreYou()
		{
			cout<<"Dog"<< endl;
		}
		Dog()
		{
		}
		Dog(int serialNumber)
		:Animal(serialNumber)
		{
		}
	};

class Bird: public Animal
{
public:
		void whoAreYou()
		{
			cout<<"Bird"<< endl;
		}
		Bird()
		{
		}
		Bird(int serialNumber)
		:Animal(serialNumber)
		{
		}
};

void showType(Animal* animal)
{
	animal->whoAreYou();
}

int main()
{
	srand(time(NULL));
	vector<Animal*> animals;
	int serialNumber=1;
	int animalType;

	for(int i=0; i<200; i++)
	{
		animalType= rand()%2;
		if (animalType==0)
		{
			animals.push_back(new Bird(serialNumber));
		}
		else if(animalType==1)
		{
			animals.push_back(new Dog(serialNumber));
		}
		serialNumber++;
	}

	for_each(animals.begin(), animals.end(), showType);

	Bird* tmpBird=0;
	Dog* tmpDog=0;

	vector<Bird*> birds;
	vector<Dog*> dogs;

	for(vector<Animal*>::iterator it=animals.begin(); it!=animals.end();advance (it,1))
	{
		tmpBird=dynamic_cast<Bird*>(*it);
		if(tmpBird)
		{
			birds.push_back(tmpBird);
			continue;
		}

		tmpDog=dynamic_cast<Dog*>(*it);
		if(tmpDog)
		{
			dogs.push_back(tmpDog);
		}
	}

	for_each(birds.begin(), birds.end(), showType);
	for_each(dogs.begin(), dogs.end(), showType);

	for(vector<Animal*>::iterator it=animals.begin(); it!=animals.end(); advance(it,1))
	{
		delete *it;
	}

	return 0;
}
