//2. Napisz analogiczn� funkcj�, kt�ra zwr�ci przemieszanego stringa (random_shuffle)
#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include<cstdlib>
#include<ctime>
using namespace std;

string shuffleString(string toShuffle)
{
	 vector<char> wyraz;

	 for (unsigned int i=0; i<toShuffle.size(); i++)
	 	{
	 		wyraz.push_back(toShuffle[i]);
	 	}

	 random_shuffle(wyraz.begin(), wyraz.end());
	 return string(wyraz.begin(), wyraz.end());
}
int main()
{
	srand(time(NULL));
	cout<< shuffleString("Antananarywa")<< endl;
	cout<< shuffleString("Antananarywa")<< endl;
	return 0;
}
