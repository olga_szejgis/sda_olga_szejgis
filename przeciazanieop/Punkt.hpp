/*
 * Punkt.hpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef PUNKT_HPP_
#define PUNKT_HPP_
#include <iostream>

class Punkt
{
	float mX;
	float mY;

public:
	Punkt(float x, float y)
	:mX(x)
	,mY(y)
	{
	}
	Punkt()
	:mX(0)
	,mY(0)
	{
	}
	Punkt(const Punkt& pkt)
	:mX(pkt.getX())
	,mY(pkt.getY())
	{
	}
	void wypisz()
	{
		std::cout<< "mX= "<< mX<< " mY=" << mY<< std::endl;
	}

	float getX() const
	{
		return mX;
	}

	void setX(float x)
	{
		mX = x;
	}

	float getY() const
	{
		return mY;
	}

	void setY(float y)
	{
		mY = y;
	}

	Punkt operator+ (const Punkt plus) const
		{
		Punkt wynik(0,0);
		wynik.setX(mX+plus.getX());
		wynik.setY(mY+plus.getY());
		return wynik;
		}

	Punkt operator+ (const float liczba) const
	{
		Punkt wynik(0,0);
		wynik.setX(mX+liczba);
		wynik.setY(mY+liczba);
		return wynik;
	}


	Punkt operator* (const float liczba) const
	{
		Punkt wynik(0,0);
		wynik.setX(mX*liczba);
		wynik.setY(mY*liczba);
		return wynik;
	}

};

Punkt operator* (const float liczba, const Punkt& pkt)
	{
	return pkt*liczba;
	}

Punkt operator+ (const float liczba, const Punkt& pkt)
	{
	return pkt+liczba;
	}

#endif /* PUNKT_HPP_ */
