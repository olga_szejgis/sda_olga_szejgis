#include "Punkt.hpp"

int main ()
{
	Punkt p1(8,9);
	p1.wypisz();
	p1=p1+2.5;
	p1.wypisz();
	Punkt p2 (2,3);
	p2=p2*3.5;
	p2.wypisz();
	p2=2+p2;
	p2.wypisz();
	Punkt p3(1,2);
	Punkt p4(3,4);
	p4=p3+p4;
	p4.wypisz();

	return 0;
}
