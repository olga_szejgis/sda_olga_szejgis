/*
 * Zbior.hpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef ZBIOR_HPP_
#define ZBIOR_HPP
#include "Punkt.hpp"

class Zbior
{
	Punkt** mPtr;
	size_t mSize;

	void resize()
	{
		if (mSize==0)
			{
				mPtr= new Punkt* [++mSize];
			}
		else
			{
				Punkt** tmp= new Punkt* [++mSize];
				for (size_t i=0; i<mSize-1; i++)//kopiowanie ze starej tablicy
				{
					tmp[i]=mPtr[i];
				}
				delete [] mPtr;
				mPtr=tmp;

			}
	}
public:
	Zbior()
:mPtr(0), mSize(0)
	{
	}
	~Zbior()
	{
		if (mSize!=0)
		{
			for (size_t i=0; i<mSize; i++)
			{
				delete mPtr[i];
			}
			delete [] mPtr;
			mSize=0;
		}
	}

	void operator+ (const Punkt& pkt)
{
	resize();
	mPtr[mSize]=new Punkt(pkt);

}
	void wypisz()
	{
		for (size_t i=0; i<mSize;i++)
		{
			mPtr[i]->wypisz();
		}
	}

};

#endif /* ZBIOR_HPP_ */
