//10. Napisz funkcj�, kt�ra zwr�ci wszystkie mo�liwe permutacje 3 liczb (next_permutation)
//
//11. Napisz funkcj�, kt�ra przyjmuje wektor cyfr, a zwraca liczb� permutacji tych cyfr, kt�re s� wielokrotno�ci� 11.

//1. Napisz funkcj�, kt�ra przyjmuje wektor cyfr, a zwraca liczb� permutacji tych cyfr, kt�re s� wielokrotno�ci� 11.

#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
using namespace std;


void permutation(int first, int second, int third)
{
	vector<int> myVector;

	myVector.push_back(first);
	myVector.push_back(second);
	myVector.push_back(third);

	sort(myVector.begin(), myVector.end());

	vector<int>::iterator it=myVector.begin();

	do
	{
		cout<< *it<< " "<< *(it+1)<< " "<< *(it+2)<< endl;
	}
	while(next_permutation(myVector.begin(), myVector.end()));

}

int permutation2(vector <int> wektor)
{
	int counter=0;
	int toPermutate=0;
	int multiple;

	sort(wektor.begin(), wektor.end());

	do
	{
		toPermutate=0;
		multiple=1;
		for(vector <int>::reverse_iterator it= wektor.rbegin(); it!=wektor.rend();advance(it,1))
		{
			toPermutate+= (*it)*multiple;
			multiple*=10;
		}
		cout << toPermutate<< endl;
		if (toPermutate%11==0)
		{
			counter++;
		}
	}
	while(next_permutation(wektor.begin(), wektor.end()));

	return counter;
}



int main()
{
	permutation(3,2,1);

	vector <int> test;
	test.push_back(5);
	test.push_back(2);
	test.push_back(3);
//	test.push_back(1);
//	test.push_back(5);
	cout<<permutation2(test);

	return 0;
}
