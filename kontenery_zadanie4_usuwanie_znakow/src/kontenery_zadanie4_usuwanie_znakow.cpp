//4. Napisz funkcj�, kt�ra usunie spacje z podanego stringa(remove i erase na vector i list).


#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <list>
using namespace std;

void removeFromList(string& toChange)
{
list <char> to_change;

for (unsigned int i=0; i<toChange.size(); i++)
	 	{
	 		to_change.push_back(toChange[i]);
	 	}
to_change.remove(' ');
toChange= string(to_change.begin(), to_change.end());
}

void removeFromVector(string& toChange)
{
vector<char> to_change;

for (unsigned int i=0; i<toChange.size(); i++)
	 	{
	 		to_change.push_back(toChange[i]);
	 	}

for(vector<char>::iterator it=to_change.begin(); it!=to_change.end(); ++it)
{
	if (*it==' ')
	{
		to_change.erase(it);
		it--;
	}
}
toChange= string(to_change.begin(), to_change.end());
}

int main()
{
	string str1="bee beee";
	removeFromVector(str1);
	cout<<str1<< endl;
	string str2= "beee   beeee beee kopytka niosa mnie";
	removeFromList(str2);
	cout << str2;

	return 0;
}
