/*
 * punkt.cpp
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */
#include "punkt.hpp"

Punkt::Punkt ()
:mX()
,mY()
{
}

int Punkt::getX() const
{
	return mX;
}

void Punkt::setX(int x)
{
	mX = x;
}

int Punkt::getY() const
{
	return mY;
}

void Punkt::setY(int y)
{
	mY = y;
}

Punkt::Punkt (int x, int y)
:mX(x)
,mY(y)
{
}

void Punkt:: wypisz()
{
	std::cout << "Wspolrzedne: ["<< mX<< ", " << mY << "]"<< std::endl;
}

void Punkt::przesun (int x, int y)
{
przesunX(x);
przesunY(y);

}
void Punkt::przesun (Punkt a)
{
przesun(a.getX(), a.getY());
}

void Punkt::przesunX (int x)
{
	if (mX+x >800)
		{
			mX=800;
		}
		else

		mX+=x;
}
void Punkt::przesunY (int y)
{
	if (mY+y >800)
		{
		mY=800;
		}
		else

		mY+=y;
}

float Punkt:: obliczOdleglosc(int x, int y)
{
return sqrt((x-mX)*(x-mX)+(y-mY)*(y-mY));
}

float Punkt::obliczOdleglosc(Punkt a)
{
	 return obliczOdleglosc(a.getX(), a.getY());
}
