
#include "KolorowyPunkt.hpp"

KolorowyPunkt::KolorowyPunkt(int x, int y, Color kolor)
:Punkt(x,y)
,mKolor(kolor)
{
}

void KolorowyPunkt:: wypisz()
{
	Punkt::wypisz();

	switch(mKolor)
	{
	case red:
		std::cout << "Czerwony "<< std::endl;
		break;
	case green:
		std::cout << "Zielony "<< std::endl;
		break;
	case black:
			std::cout << "Czarny "<< std::endl;
			break;
	default:
		std::cout << "Nieznany kolor "<< std::endl;
	}
}
