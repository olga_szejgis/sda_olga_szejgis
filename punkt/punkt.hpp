/*
 * punkt.hpp
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */

#ifndef PUNKT_HPP_
#define PUNKT_HPP_
#include <iostream>
#include <cmath>

class Punkt
{
	int mX;
	int mY;
public:
	Punkt ();
	Punkt (int x, int y);
	int getX() const;
	void setX(int x);
	int getY() const;
	void setY(int y);
	void wypisz();
	void przesun (int x, int y);
	void przesun (Punkt a);
	void przesunX (int x);
	void przesunY (int y);
	float obliczOdleglosc(int x, int y);
	float obliczOdleglosc(Punkt a);
};




#endif /* PUNKT_HPP_ */
