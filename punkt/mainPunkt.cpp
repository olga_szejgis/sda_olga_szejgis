/*
 * mainPunkt.cpp
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */
#include "punkt.hpp"
#include "kolorowyPunkt.hpp"

int main()
{
	Punkt p1(4,3);
	Punkt p2(8,7);

	p2.przesun(p1);

	p2.wypisz();
	std:: cout<< p1.obliczOdleglosc(p2)<< std::endl;

	KolorowyPunkt p3 (3,5,KolorowyPunkt::red);
	p3.wypisz();

	return 0;
}



