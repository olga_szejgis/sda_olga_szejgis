/*
 * kolorowyPunkt.hpp
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */

#ifndef KOLOROWYPUNKT_HPP_
#define KOLOROWYPUNKT_HPP_
#include "punkt.hpp"

class KolorowyPunkt: public Punkt
{
public:
enum Color
{
	red,
	green,
	black
};

KolorowyPunkt(int x, int y, Color kolor);
void wypisz ();

private:
Color mKolor;
};



#endif /* KOLOROWYPUNKT_HPP_ */
