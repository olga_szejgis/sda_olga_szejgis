// Napisz funkcj�, kt�ra przyjmuje dwa stringi a nast�pnie zwraca vector ich wsp�lnych liter.

#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
using namespace std;

vector<char> commonChar(string first, string second)
{
vector <char> common;
for (unsigned int i=0; i<first.size(); i++)
{
	for (unsigned int k=0; k<second.size(); k++)
	{
		if (first[i]== second[k])
		{
			common.push_back(second[k]);
		}
	}
}
	vector<char>::iterator it;
	sort(common.begin(), common.end());
	it= unique(common.begin(), common.end());
	common.resize(distance(common.begin(), it));

return common;
}

int main()
{
	vector<char> common= commonChar("kaczuszka", "stegozaur");
	for (vector<char>::iterator it=common.begin(); it!=common.end(); advance(it, 1))
			{
				cout << *it<< endl;
			}

	return 0;
}
