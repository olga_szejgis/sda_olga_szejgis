//============================================================================
// Name        : rzymskie.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>

using namespace std;

int main(int argc, char* argv[])
{
  string rzym;
  int wynik=0;
  cout << "Podaj rzymska liczbe " << flush;
  getline (cin, rzym);

  if (rzym.empty())
    {
      cout << "Koniec " << endl;
      return 0;
    }
  for (unsigned i=0; i<rzym.length(); i++)
    {
      char znak= rzym[i];
      char nastepnyZnak;

      if(i+1 < rzym.length())
	{
	  nastepnyZnak= rzym[i+1];
	}
      else
	{
	  nastepnyZnak= '\0';
	}

      if ((znak== 'I') && (nastepnyZnak == 'V'|| nastepnyZnak== 'X'))
             {
               wynik-=1;
             }
      else if (znak== 'I')
	{
	 wynik+=1;
	}

      else if (znak== 'V')
	{
	  wynik+=5;
	}
      else if (znak== 'X')
	{
	  wynik+=10;
	}
      else if (znak == 'L')
	{
	  wynik += 50;
	}
      else if (znak == 'C')
	{
	  wynik+=100;
	}
      else if (znak == 'D')
	{
	  wynik+=500;
	}
      else if (znak == 'M')
	{
	  wynik+=1000;
	}
      else if ((znak== 'I') && (nastepnyZnak == 'V'|| nastepnyZnak== 'X'))
     	{
     	  wynik-=1;
     	}

    }
  cout << "W liczbach arabskich: " << wynik << endl;

	return 0;
}
