//============================================================================
// Name        : silnia.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
int
silnia (int n)
{
  if (n == 1)
    {
      return 1;
    }
  else
    {
      return n = n * silnia (n - 1);
    }
}

int
main ()
{
  int liczba;
  std::cout << "Podaj liczbe: " << std::endl;
  std::cin >> liczba;
  std::cout << "Silnia rekurencyjnie =" << silnia (liczba) << std::endl;

  int wynik = 1;
  while (liczba > 0)
    {
      wynik *= liczba;
      liczba--;
    }
  std::cout << "Silnia iteracyjnie =" << wynik << std::endl;

  return 0;
}
