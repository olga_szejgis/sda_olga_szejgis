/*
 * main.cpp
 *
 *  Created on: Mar 23, 2017
 *      Author: orlik
 */

#include <iostream>

using namespace std;

class Poczatek
{
protected:
	int mX;

public:
	void przedstawSie()
	{
		cout<<"Jestem Poczatek \n";
	}

	Poczatek()
	: mX(0)
	{
		cout<<"Poczatek\n";
	}
	Poczatek(int x)
	{
		mX=x;
	}


	~Poczatek()
	{
		cout<<"~Poczatek\n";
	}

	void ustawX()
	{
		mX = 1;
	}

	void wypiszX()
	{
		std::cout<<"Poczatek::X="<<mX<<endl;
	}

};

class Srodek : public Poczatek
{
protected:
	double mX;

public:
	void przedstawSie()
	{
		cout<<"Jestem Srodek \n";
	}

	Srodek()
	: mX(10.31)
	{
		cout<<"Srodek\n";
	}

	~Srodek()
	{
		cout<<"~Srodek\n";
	}

	void ustawX()
	{
		Poczatek::mX = 11;
		mX = 12.31;
	}

	void wypiszX()
	{
		std::cout<<"Poczatek::X="<<Poczatek::mX<<"Srodek::X="<<mX<<endl;
	}
};

class Koniec : public Srodek
{
protected:
	int mX;

public:
	void przedstawSie()
	{
		cout<<"Jestem Koniec \n";
	}

	Koniec()
	: mX(100)
	{
		cout<<"Koniec\n";
	}

	~Koniec()
	{
		cout<<"~Koniec\n";
	}

	void ustawX()
	{
		Poczatek::mX = 111;
		Srodek::Poczatek::mX = 112.432;
		mX = 113;
	}

	void wypiszX()
	{
		std::cout<<"Poczatek::X="<<Poczatek::mX<<"Srodek::X="<<Srodek::mX<<"Koniec::X="<<mX<<endl;
	}

};


int main()
{
	Poczatek poczatek1(6);
	Poczatek *pocz = new Poczatek();
	Srodek *srod = new Srodek();
	Koniec *koni = new Koniec();

	pocz->przedstawSie();
	srod->przedstawSie();
	koni->przedstawSie();

	koni->Poczatek::przedstawSie();

	pocz->wypiszX();
	srod->wypiszX();
	koni->wypiszX();

	pocz->ustawX();
	srod->ustawX();
	koni->ustawX();

	pocz->wypiszX();
	srod->wypiszX();
	koni->wypiszX();

	pocz->wypiszX();

	delete pocz;
	delete srod;
	delete koni;

	return 0;
}
