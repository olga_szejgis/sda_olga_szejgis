//============================================================================
// Name        : kabala.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

//A       B       C       D       E       F       G       H       I       K
//1       2       3       4       5       6       7       8       9       10
//L       M       N       O       P       Q       R       S       T       V
//20      30      40      50      60      70      80      90      100     200
//X       Y       Z
//300     400     500

#include <iostream>
using namespace std;

int main()
{
	char litery[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
			'i', 'k', 'l' , 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'v', 'x', 'y', 'z'};

	int liczby[]= { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 200, 300, 400, 500};

	string data;
	cout << "Podaj wyraz: " << endl;
	cin >> data;

	int wynik=0;

	for (int i=0; i<data.length(); ++i)
	{
		for (int k=0; k<23; ++k)
		{
			if (data[i]== litery[k])
			{
				wynik+= liczby[k];
			}
		}
	}
	cout << wynik << endl;

	return 0;
}
